package com.MakemyBeauty.makemybeauty.Services;

import android.content.Context;
import android.os.AsyncTask;

import com.MakemyBeauty.makemybeauty.Model.UC;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static com.MakemyBeauty.makemybeauty.Utilities.AppConstant.url_base;

public class CartCount extends AsyncTask<Context, Void, Integer> {

    Integer result = 200;
    UC ht_inst = UC.getInstance();

    @Override
    protected Integer doInBackground(Context... params) {

        BufferedReader reader = null;
        try {
            // Create Apache HttpClient
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(url_base+"cartCount");

            List<NameValuePair> postRequestParams = new ArrayList<>();

            //System.out.println("jyo cartcount sending paramter"+"mobile"+ht_inst.getUser().getMobile()+"token"+ht_inst.getUser().getUserToken());
            postRequestParams.add(new BasicNameValuePair("mobile", String.valueOf(ht_inst.getUser().getMobile())));
            postRequestParams.add(new BasicNameValuePair("token", String.valueOf(ht_inst.getUser().getUserToken())));
            postRequestParams.add(new BasicNameValuePair("user_id", String.valueOf(ht_inst.getUser().getUserId())));
            postRequest.setEntity(new UrlEncodedFormEntity(postRequestParams));

            HttpResponse httpResponse = httpclient.execute(postRequest);

            String line;
            reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            if((line = reader.readLine())!=null){
                System.out.println("jyo add cartcount response "+line);
            }

            parseResult(line);

        } catch (Exception e) {
            System.out.println("jyo"+e.getCause());
            e.printStackTrace();
            System.out.println("jyo exception occurred ");
        }finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }
    /**
     * Parsing the feed results and get the list
     *
     * @param response
     */
    public void parseResult(String response) {

        System.out.println("jyo parse start");
        try {
            JSONObject post = new JSONObject(response);

        }catch (Exception e) {
            System.out.println("jyo cartcount failed parse response");
        }
    }

    @Override
    protected void onPostExecute(Integer result) {
        // Download complete. Lets update UI

        if (result == 1) {

            //mGridAdapter.setGridData(mGridData);
        } else {
            // Toast.makeText(LocationViewActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}




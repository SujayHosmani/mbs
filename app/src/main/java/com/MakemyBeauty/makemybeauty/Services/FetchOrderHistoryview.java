package com.MakemyBeauty.makemybeauty.Services;

import android.content.Context;
import android.os.AsyncTask;

import com.MakemyBeauty.makemybeauty.Model.Order;
import com.MakemyBeauty.makemybeauty.Model.Tables;
import com.MakemyBeauty.makemybeauty.Model.UC;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static com.MakemyBeauty.makemybeauty.Utilities.AppConstant.url_base;

public class FetchOrderHistoryview extends AsyncTask<Context, Void, Integer> {

    Integer result = 200;
    UC ht_inst = UC.getInstance();

    @Override
    protected Integer doInBackground(Context... contexts) {

        BufferedReader reader = null;
        try {
            // Create Apache HttpClient
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(url_base+"orderHistoryView");

            List<NameValuePair> postRequestParams = new ArrayList<>();

            // System.out.println("jyo task sending paramter"+"mobile"+ht_inst.getUser().getMobile()+"token"+ht_inst.getUser().getUserToken());
            postRequestParams.add(new BasicNameValuePair("mobile", String.valueOf(ht_inst.getUser().getMobile())));
            postRequestParams.add(new BasicNameValuePair("token", String.valueOf(ht_inst.getUser().getUserToken())));
            postRequestParams.add(new BasicNameValuePair("user_id", String.valueOf(ht_inst.getUser().getUserId())));

            postRequest.setEntity(new UrlEncodedFormEntity(postRequestParams));

            HttpResponse httpResponse = httpclient.execute(postRequest);

            String line;

            reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            if((line = reader.readLine())!=null){
                System.out.println("jyo get fetch my orderHistory response "+line);
            }
            //  System.out.println("jyo get fetch my orderHistory response "+line);
            parseResult(line);

        } catch (Exception e) {
            System.out.println("jyo"+e.getCause());
            e.printStackTrace();
            System.out.println("jyo exception occurred ");
        }finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    /**
     * Parsing the feed results and get the list
     *
     * @param response
     */
    private void parseResult(String response) {
        try {
            JSONObject post = new JSONObject(response);
            result = post.getInt("errorCode");

            JSONArray post1 = post.getJSONArray("data");

            ht_inst.getOrderHistorylist().clear();

            System.out.println("jyo 1 "+post.length());
            for (int i = 0; i < post1.length(); i++) {
                JSONObject item = post1.getJSONObject(i);

                Tables catItem = new Tables();
                catItem.setOrder_id(item.getInt("order_id"));
                // System.out.println("jyo order id1 "+catItem.getOrder_id());
                catItem.setTrnsx_id(item.getString("trnx_id"));
                catItem.setGrand_price(item.getString("grand_price"));

                catItem.setGrand_price(item.getString("order_time"));

                catItem.setUser_id(item.getInt("user_id"));
                catItem.setDop(item.getString("dop"));
                catItem.setName(item.getString("name"));
                catItem.setAddress(item.getString("address"));
                catItem.setMobile(item.getString("mobile"));
                catItem.setEmail(item.getString("email"));
                catItem.setTotal_amt(item.getString("total_amt"));
                catItem.setGst(item.getString("gst"));
                catItem.setDelivery_charge(item.getString("delivery_charge"));
                catItem.setPayment_mode(item.getString("payment_mode"));
                catItem.setPayable_amount(item.getString("payable_amount"));
                catItem.setDue_amt(item.getString("due_amt"));
                catItem.setStatus(item.getString("status"));
                catItem.setUpdated_on(item.getString("updated_on"));
                catItem.setCreated_on(item.getString("created_on"));






                ht_inst.getOrderHistorylist().add(catItem);
                ht_inst.setAddtable(catItem);


            }

            System.out.println("jyo number of orderHistory fetched is "+ht_inst.getOrderHistorylist().size());
            // System.out.println("jyo number of orderHistory added "+ht_inst.getAddtable().getOrder_id());

        }catch (Exception e) {

            System.out.println("jyo failed  parse fetch category");

        }

    }

    @Override
    protected void onPostExecute(Integer result) {
        // Download complete. Lets update UI

        if (result == 1) {

            //mGridAdapter.setGridData(mGridData);
        } else {
            // Toast.makeText(LocationViewActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}

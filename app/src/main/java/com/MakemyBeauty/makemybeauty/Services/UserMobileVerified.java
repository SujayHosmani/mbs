package com.MakemyBeauty.makemybeauty.Services;

import android.os.AsyncTask;

import com.MakemyBeauty.makemybeauty.Model.UC;
import com.MakemyBeauty.makemybeauty.Model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static com.MakemyBeauty.makemybeauty.Utilities.AppConstant.url_base;


public class UserMobileVerified  extends AsyncTask<String, Void, Integer> {
    Integer result = 201;
    @Override
    protected Integer doInBackground(String... params) {

        BufferedReader reader = null;
        try {
            // Create Apache HttpClient
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(url_base+"validateMobileReg");

            UC dg_inst = UC.getInstance();

            User inParam = new User();
            System.out.println("jyo mobile"+dg_inst.getUser().getMobile());
            inParam.setMobile(dg_inst.getUser().getMobile());
            // inParam.setMobileVerified(true);
            Gson gson = new GsonBuilder().disableHtmlEscaping().create();
            //postRequest.setEntity(new StringEntity(json, "UTF-8"));
            StringEntity stringEntity = new StringEntity(gson.toJson(inParam));
            postRequest.setEntity(stringEntity);
            postRequest.setHeader("Content-Type", "application/json");

            HttpResponse httpResponse = httpclient.execute(postRequest);

            String line;
            reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            if((line = reader.readLine())!=null){
                System.out.println("jyo mobile verified status "+line);
            }
           /* JSONObject jsonObject1 = new JSONObject(line);

            Integer statusCode = jsonObject1.optInt("StatusCode");

            if (statusCode == 201) {*/
            parseResult(line);
            // result = 201;
            //}

        } catch (Exception e) {
            System.out.println("jyo"+e.getCause());
            e.printStackTrace();
            System.out.println("jyo exception occurred ");
        }finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }
    private void parseResult(String response) {
        try {
            System.out.println("jyo mobverfied calling parse ");
            JSONObject post = new JSONObject(response);
            result = post.getInt("errorCode");

        } catch (Exception e) {
            System.out.println("jyo failed  parse");
        }
    }

    @Override
    protected void onPostExecute(Integer result) {
        // Download complete. Lets update UI

        if (result == 1) {

            //mGridAdapter.setGridData(mGridData);
        } else {
            // Toast.makeText(LocationViewActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}



package com.MakemyBeauty.makemybeauty.Services;

import android.content.Context;
import android.os.AsyncTask;

import com.MakemyBeauty.makemybeauty.Activities.HomeActivity;
import com.MakemyBeauty.makemybeauty.Activities.ServicesActivity;
import com.MakemyBeauty.makemybeauty.Model.Category;
import com.MakemyBeauty.makemybeauty.Model.UC;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static com.MakemyBeauty.makemybeauty.Utilities.AppConstant.url_base;

public class FetchServices extends AsyncTask<Integer, Void, Integer> {

    //ServicesActivity mContext;
    Integer result = 200;
    UC ht_inst = UC.getInstance();
    public int id1,id2;

    @Override
    protected Integer doInBackground(Integer... params) {

        id1=params[0];
        id2=params[1];

        BufferedReader reader = null;

        try {
            // Create Apache HttpClient
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(url_base+"fetchcategoryService");

            List<NameValuePair> postRequestParams = new ArrayList<>();
           // mContext = (ServicesActivity) params[0];

            System.out.println("jyo Category sending paramter"+"mobile"+ht_inst.getUser().getMobile()+"token"+ht_inst.getUser().getUserToken());

            postRequestParams.add(new BasicNameValuePair("mobile", String.valueOf(ht_inst.getUser().getMobile())));
            postRequestParams.add(new BasicNameValuePair("token", String.valueOf(ht_inst.getUser().getUserToken())));
            postRequestParams.add(new BasicNameValuePair("category", String.valueOf(id1)));
            postRequestParams.add(new BasicNameValuePair("subcategory", String.valueOf(id2)));
            postRequest.setEntity(new UrlEncodedFormEntity(postRequestParams));

            HttpResponse httpResponse = httpclient.execute(postRequest);

            String line;
            reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

            if((line = reader.readLine())!=null){
                System.out.println("jyo get fetch my service response "+line);
            }

            System.out.println("jyo get fetch my service response "+line);
            parseResult(line);

        } catch (Exception e) {
            System.out.println("jyo"+e.getCause());
            e.printStackTrace();
            System.out.println("jyo exception occurred ");
        }finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    /**
     * Parsing the feed results and get the list
     *
     * @param response
     */
    private void parseResult(String response) {
        try {
            JSONObject post = new JSONObject(response);
            result = post.getInt("errorCode");

            JSONArray post1 = post.getJSONArray("image");

            UC.getInstance().setProductList2(null);
            //ag_inst.getAllproductlist().clear();
            ArrayList<Category> categories=new ArrayList<>();

          //  ht_inst.getCategoryList().clear();

            System.out.println("jyo 1 "+post.length());
            for (int i = 0; i < post1.length(); i++) {
                JSONObject item = post1.getJSONObject(i);

                Category catItem = new Category();


                catItem.setCatId(item.getInt("category"));
                catItem.setSubcatId(item.getInt("subcategory"));
                catItem.setServiceId(item.getInt("service_id"));
                catItem.setServiceName(item.getString("services"));
                catItem.setPrice(item.getString("price"));

                ht_inst.setCategory(catItem);
                categories.add(catItem);

            }
            UC.getInstance().setProductList2(categories);

            //  System.out.println("jyo number of category fetched is "+ht_inst.getCategoryList().size());
        }
        catch (Exception e) {
            System.out.println("jyo failed  parse fetch category");
        }
    }

    @Override
    protected void onPostExecute(Integer result) {
        // Download complete. Lets update UI

        if (result == 1) {

            //mGridAdapter.setGridData(mGridData);
        } else {
            // Toast.makeText(LocationViewActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}
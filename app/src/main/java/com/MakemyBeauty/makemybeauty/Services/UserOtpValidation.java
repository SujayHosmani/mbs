package com.MakemyBeauty.makemybeauty.Services;

import android.os.AsyncTask;

import com.MakemyBeauty.makemybeauty.Model.UC;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import static com.MakemyBeauty.makemybeauty.Utilities.AppConstant.url_base;


public class UserOtpValidation extends AsyncTask<String, Void, Integer> {

    UC dg_inst = UC.getInstance();
    Integer result = 200;

    @Override
    protected Integer doInBackground(String... params) {

        BufferedReader reader = null;
        try {
            // Create Apache HttpClient
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(url_base + "verifyOtpRegistration");

            UC dg_inst = UC.getInstance();

            List<NameValuePair> postRequestParams = new ArrayList<>();

            System.out.println("jyo sending mob"+dg_inst.getUser().getMobile()+"jyo otp"+dg_inst.getUser().getOtp());
            postRequestParams.add(new BasicNameValuePair("mobile", String.valueOf(dg_inst.getUser().getMobile())));
            postRequestParams.add(new BasicNameValuePair("otp", String.valueOf(dg_inst.getUser().getOtp())));

            postRequest.setEntity(new UrlEncodedFormEntity(postRequestParams));

            HttpResponse httpResponse = httpclient.execute(postRequest);

           /* System.out.println("jyo get registration called ");
            User inParam = new User();

            inParam.setName(params[1]);
            inParam.setAddress(params[2]);
            inParam.setEmail(params[3]);
            inParam.setPassword(params[4]);
            inParam.setDob(params[5]);
            inParam.setGender(params[6]);*/


           /* Gson gson = new GsonBuilder().disableHtmlEscaping().create();
            //postRequest.setEntity(new StringEntity(json, "UTF-8"));
            StringEntity stringEntity = new StringEntity(gson.toJson(dg_inst.getUser()));
            postRequest.setEntity(stringEntity);
            postRequest.setHeader("Content-Type", "application/json");

            HttpResponse httpResponse = httpclient.execute(postRequest);*/

            String line;
            reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            if ((line = reader.readLine()) != null) {
                System.out.println("jyo get otp response " + line);
            }

            parseResult(line);

        } catch (Exception e) {
            System.out.println("jyo" + e.getCause());
            e.printStackTrace();
            System.out.println("jyo exception occurred ");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }
    /**
     * Parsing the feed results and get the list
     *
     * @param response
     */
    private void parseResult(String response) {
        try {
            System.out.println("jyo otp verfied calling parse");
            JSONObject post = new JSONObject(response);
            result = post.getInt("errorCode");

            dg_inst.getUser().setUserId(post.getInt("user_id"));
            System.out.println("jyo id"+dg_inst.getUser().getUserId());
            dg_inst.getUser().setUserToken(post.getString("token"));
            System.out.println("jyo token"+dg_inst.getUser().getUserToken());
            dg_inst.getUser().setStatus(post.getString("status"));
            System.out.println("jyo status"+dg_inst.getUser().getStatus());



        } catch (Exception e) {
            System.out.println("jyo failed login parse");
        }

    }

    @Override
    protected void onPostExecute(Integer result) {
        // Download complete. Lets update UI

        if (result == 1) {

            //mGridAdapter.setGridData(mGridData);
        } else {
            // Toast.makeText(LocationViewActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}





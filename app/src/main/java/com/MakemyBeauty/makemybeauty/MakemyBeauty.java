package com.MakemyBeauty.makemybeauty;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

public class MakemyBeauty extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}

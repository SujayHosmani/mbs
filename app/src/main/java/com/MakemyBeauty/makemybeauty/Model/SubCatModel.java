package com.MakemyBeauty.makemybeauty.Model;

public class SubCatModel {
    String subcategory_id,category,subcategory;

    public SubCatModel() {
    }

    public SubCatModel(String subcategory_id, String category, String subcategory) {
        this.subcategory_id = subcategory_id;
        this.category = category;
        this.subcategory = subcategory;
    }

    public String getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(String subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }
}

package com.MakemyBeauty.makemybeauty.Model;

public class Service {

    private String Pro_name;
    private String price;
    private String Pro_image;
    private int pro_id;
    private String image;
    private String title;
    private int cart_id;
    private Double cart_total;
    private Double grand_total;
    private String order_status;
    private int order_pid;
    private String order_time;
    private String category;
    private Integer quantity = 0;

    public String getPro_name() {
        return Pro_name;
    }

    public void setPro_name(String pro_name) {
        Pro_name = pro_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPro_image() {
        return Pro_image;
    }

    public void setPro_image(String pro_image) {
        Pro_image = pro_image;
    }

    public int getPro_id() {
        return pro_id;
    }

    public void setPro_id(int pro_id) {
        this.pro_id = pro_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCart_id() {
        return cart_id;
    }

    public void setCart_id(int cart_id) {
        this.cart_id = cart_id;
    }

    public Double getCart_total() {
        return cart_total;
    }

    public void setCart_total(Double cart_total) {
        this.cart_total = cart_total;
    }

    public Double getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(Double grand_total) {
        this.grand_total = grand_total;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public int getOrder_pid() {
        return order_pid;
    }

    public void setOrder_pid(int order_pid) {
        this.order_pid = order_pid;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}

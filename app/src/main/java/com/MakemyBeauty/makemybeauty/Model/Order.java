package com.MakemyBeauty.makemybeauty.Model;

public class Order {
    private String st_title;
    private Integer st_qnt;
    private String date;
    private String trnsx_id;
    private String grand_price;
    private String order_time;
    private Integer order_id;
    private String order_status;

    public String getSt_title() {
        return st_title;
    }

    public void setSt_title(String st_title) {
        this.st_title = st_title;
    }

    public Integer getSt_qnt() {
        return st_qnt;
    }

    public void setSt_qnt(Integer st_qnt) {
        this.st_qnt = st_qnt;
    }
    public String getDate() {
        return date;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getTrnsx_id() {
        return trnsx_id;
    }

    public void setTrnsx_id(String trnsx_id) {
        this.trnsx_id = trnsx_id;
    }

    public String getGrand_price() {
        return grand_price;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public Integer getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }

    public void setGrand_price(String grand_price) {
        this.grand_price = grand_price;
    }

    public void setDate(String date) {
        this.date = date;
    }

}

package com.MakemyBeauty.makemybeauty.Model;

public class ValidateOtpModel {
    String message,user_id,token,status;
    int errorCode;

    public ValidateOtpModel() {
    }

    public ValidateOtpModel(String message, String user_id, String token, String status, int errorCode) {
        this.message = message;
        this.user_id = user_id;
        this.token = token;
        this.status = status;
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}

package com.MakemyBeauty.makemybeauty.Model;

public class ViewCartModel {
    String c_id,cart_id,user_id,s_id,subcategory_id,s_name,s_image,s_price,pro_qnty,delivery_type,address,pin,grand_price,latitude,longitude,city,updated_on,created_on;
    String status,vendor_ph, vendor_id,vendor_name,salon_name;

    public ViewCartModel() {
    }

    public ViewCartModel(String c_id, String cart_id, String user_id, String s_id, String subcategory_id, String s_name, String s_image, String s_price, String pro_qnty, String delivery_type, String address, String pin, String grand_price, String latitude, String longitude, String city, String updated_on, String created_on, String status, String vendor_ph, String vendor_id, String vendor_name, String salon_name) {
        this.c_id = c_id;
        this.cart_id = cart_id;
        this.user_id = user_id;
        this.s_id = s_id;
        this.subcategory_id = subcategory_id;
        this.s_name = s_name;
        this.s_image = s_image;
        this.s_price = s_price;
        this.pro_qnty = pro_qnty;
        this.delivery_type = delivery_type;
        this.address = address;
        this.pin = pin;
        this.grand_price = grand_price;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        this.updated_on = updated_on;
        this.created_on = created_on;
        this.status = status;
        this.vendor_ph = vendor_ph;
        this.vendor_id = vendor_id;
        this.vendor_name = vendor_name;
        this.salon_name = salon_name;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(String subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public String getS_image() {
        return s_image;
    }

    public void setS_image(String s_image) {
        this.s_image = s_image;
    }

    public String getS_price() {
        return s_price;
    }

    public void setS_price(String s_price) {
        this.s_price = s_price;
    }

    public String getPro_qnty() {
        return pro_qnty;
    }

    public void setPro_qnty(String pro_qnty) {
        this.pro_qnty = pro_qnty;
    }

    public String getDelivery_type() {
        return delivery_type;
    }

    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getGrand_price() {
        return grand_price;
    }

    public void setGrand_price(String grand_price) {
        this.grand_price = grand_price;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVendor_ph() {
        return vendor_ph;
    }

    public void setVendor_ph(String vendor_ph) {
        this.vendor_ph = vendor_ph;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getSalon_name() {
        return salon_name;
    }

    public void setSalon_name(String salon_name) {
        this.salon_name = salon_name;
    }
}

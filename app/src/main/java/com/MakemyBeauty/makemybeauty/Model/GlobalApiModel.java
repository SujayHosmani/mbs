package com.MakemyBeauty.makemybeauty.Model;

import java.util.ArrayList;

public class GlobalApiModel<T> {
    String message;
    int errorCode;
    ArrayList<T> data;

    public GlobalApiModel() {
    }

    public GlobalApiModel(String message, int errorCode, ArrayList<T> data) {
        this.message = message;
        this.errorCode = errorCode;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public ArrayList<T> getData() {
        return data;
    }

    public void setData(ArrayList<T> data) {
        this.data = data;
    }
}

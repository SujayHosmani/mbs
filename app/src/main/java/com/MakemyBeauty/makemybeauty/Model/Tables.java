package com.MakemyBeauty.makemybeauty.Model;

public class Tables {
    private String st_title;
    private Integer st_qnt;
    private String date;
    private String trnsx_id;
    private String grand_price;
    private String order_time;
    private Integer order_id;
    private String order_status;

    //Todo: added for order historyk
    private int user_id;
    private String dop;
    private String name;
    private String address;
    private String mobile;
    private String email;
    private String total_amt;
    private String gst;
    private String delivery_charge;
    private String payment_mode;
    private String payable_amount;
    private String due_amt;
    private String status;
    private String updated_on;
    private String created_on;






    public String getSt_title() {
        return st_title;
    }

    public void setSt_title(String st_title) {
        this.st_title = st_title;
    }

    public Integer getSt_qnt() {
        return st_qnt;
    }

    public void setSt_qnt(Integer st_qnt) {
        this.st_qnt = st_qnt;
    }
    public String getDate() {
        return date;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getTrnsx_id() {
        return trnsx_id;
    }

    public void setTrnsx_id(String trnsx_id) {
        this.trnsx_id = trnsx_id;
    }

    public String getGrand_price() {
        return grand_price;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public Integer getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }

    public void setGrand_price(String grand_price) {
        this.grand_price = grand_price;
    }

    public void setDate(String date) {
        this.date = date;
    }

    //Todo: added for order historyk


    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getDop() {
        return dop;
    }

    public void setDop(String dop) {
        this.dop = dop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTotal_amt() {
        return total_amt;
    }

    public void setTotal_amt(String total_amt) {
        this.total_amt = total_amt;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(String delivery_charge) {
        this.delivery_charge = delivery_charge;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getPayable_amount() {
        return payable_amount;
    }

    public void setPayable_amount(String payable_amount) {
        this.payable_amount = payable_amount;
    }

    public String getDue_amt() {
        return due_amt;
    }

    public void setDue_amt(String due_amt) {
        this.due_amt = due_amt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }
}

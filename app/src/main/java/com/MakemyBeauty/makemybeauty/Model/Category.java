package com.MakemyBeauty.makemybeauty.Model;

public class Category extends Service{

    private Integer catId;

    private String catName;
    private String catImage;
    private Integer subcatId;

    private String subcatName;
    private String subcatImage;
    private String sliderImage;
    private String category;
    private Integer serviceId;
    private String price;
    private String serviceName;
    private String serviceImage;

    //Todo: For Fetching Service Passing Sub_catId
    private Integer sub_catId;
    private Integer catIdd;

    public Integer getCatIdd() {
        return catIdd;
    }

    public void setCatIdd(Integer catIdd) {
        this.catIdd = catIdd;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }



        public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatImage() {
        return catImage;
    }

    public void setCatImage(String catImage) {
        this.catImage = catImage;
    }

    public Integer getSub_catId() {
        return sub_catId;
    }

    public void setSub_catId(Integer sub_catId) {
        this.sub_catId = sub_catId;
    }

        public Integer getSubcatId() {
        return subcatId;
    }

    public void setSubcatId(Integer subcatId) {
        this.subcatId = subcatId;
    }

    public String getSubcatName() {
        return subcatName;
    }

    public void setSubcatName(String subcatName) {
        this.subcatName = subcatName;
    }

    public String getSubcatImage() {
        return subcatImage;
    }

    public void setSubcatImage(String subcatImage) {
        this.subcatImage = subcatImage;
    }

    public String getSliderImage() {
        return sliderImage;
    }

    public void setSliderImage(String sliderImage) {
        this.sliderImage = sliderImage;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceImage() {
        return serviceImage;
    }

    public void setServiceImage(String serviceImage) {
        this.serviceImage = serviceImage;
    }
}

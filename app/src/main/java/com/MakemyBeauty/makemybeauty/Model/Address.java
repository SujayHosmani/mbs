package com.MakemyBeauty.makemybeauty.Model;

public class Address  {

    private String address;
    private String locality;
    private String city;
    private String state;
    private String country;
    private String landmark;
    private String pincode;
    private String loc_email;
    private String loc_mobile;
    private String loc_map;
    private Integer user_pin_code;
    private double del_charge;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getLoc_email() {
        return loc_email;
    }

    public void setLoc_email(String loc_email) {
        this.loc_email = loc_email;
    }

    public String getLoc_mobile() {
        return loc_mobile;
    }

    public void setLoc_mobile(String loc_mobile) {
        this.loc_mobile = loc_mobile;
    }

    public String getLoc_map() {
        return loc_map;
    }

    public void setLoc_map(String loc_map) {
        this.loc_map = loc_map;
    }

    public Integer getUser_pin_code() {
        return user_pin_code;
    }

    public void setUser_pin_code(Integer user_pin_code) {
        this.user_pin_code = user_pin_code;
    }

    public double getDel_charge() {
        return del_charge;
    }

    public void setDel_charge(double del_charge) {
        this.del_charge = del_charge;
    }
}

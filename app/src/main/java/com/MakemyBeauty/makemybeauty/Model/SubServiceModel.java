package com.MakemyBeauty.makemybeauty.Model;

public class SubServiceModel {
    String service_id,category,subcategory, services,price;

    public SubServiceModel() {
    }

    public SubServiceModel(String service_id, String category, String subcategory, String services, String price) {
        this.service_id = service_id;
        this.category = category;
        this.subcategory = subcategory;
        this.services = services;
        this.price = price;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}

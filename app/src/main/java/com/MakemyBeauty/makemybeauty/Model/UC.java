package com.MakemyBeauty.makemybeauty.Model;

import android.location.Address;

import java.util.ArrayList;
import java.util.HashMap;

public class UC {
    private User user;
    private Address address;
    private static UC omInst = null;
    private HashMap<String, ArrayList<Products>> proMapping;
    private HashMap<Integer, ArrayList<Category>> subcatMapping;
    private ArrayList<Products> mProductList;
    private ArrayList<Products> proorderList;
    public ArrayList<Tables>orderHistorylist;
    private ArrayList<Category>categoryList;
    private ArrayList<Category>categoryList2;
    private ArrayList<Category>sliderList;
    public ArrayList<Products> cartItems;
    public ArrayList<Address>addresslist;
    private Category category;
    private Products addproducts;
    public Integer cartqunatity = 0;
    public ArrayList<Tables>tablelist;
   // private ArrayList<Plan>planArrayList;
    private Boolean isInitDone;
    private Tables addtable;
    public ArrayList<Category> ProductList2;

    private int catId;
    private int subcatId;

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public int getSubcatId() {
        return subcatId;
    }

    public void setSubcatId(int subcatId) {
        this.subcatId = subcatId;
    }

    public static UC getInstance() {
        if (omInst == null) {
            omInst = new UC();
        }

        return omInst;
    }

    public static UC getOmInst() {
        return omInst;
    }

    public static void setOmInst(UC omInst) {
        UC.omInst = omInst;
    }

    private UC() {
        user = new User();
        proMapping = new HashMap<String, ArrayList<Products>>();
        subcatMapping = new HashMap<Integer, ArrayList<Category>>();
        categoryList2 = new ArrayList<Category>();
        categoryList = new ArrayList<Category>();
        addresslist = new ArrayList<Address>();
        mProductList = new ArrayList<Products>();
        sliderList = new ArrayList<Category>();
        category = new Category();
        cartItems = new ArrayList<Products>();
        addproducts = new Products();
        tablelist = new ArrayList<Tables>();
        addtable = new Tables();
      //  address = new Address();
        proorderList = new ArrayList<Products>();
       // planArrayList = new ArrayList<Plan>();
        orderHistorylist = new ArrayList<Tables>();
        isInitDone = false;
        // dummyData();
    }
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public Tables getAddtable() {
        return addtable;
    }

    public void setAddtable(Tables addtable) {
        this.addtable = addtable;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public HashMap<String, ArrayList<Products>> getProMapping() {
        return proMapping;
    }

    public void setProMapping(HashMap<String, ArrayList<Products>> proMapping) {
        this.proMapping = proMapping;
    }

    public ArrayList<Address> getAddresslist() {
        return addresslist;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setAddresslist(ArrayList<Address> addresslist) {
        this.addresslist = addresslist;
    }

    public ArrayList<Category> getCategoryList() {
        return categoryList;
    }

    public ArrayList<Category> getCategoryList2() {
        return categoryList2;
    }

    public void setCategoryList2(ArrayList<Category> categoryList2) {
        this.categoryList2 = categoryList2;
    }

    public Products getAddproducts() {
        return addproducts;
    }

    public ArrayList<Products> getProorderList() {
        return proorderList;
    }

    public void setProorderList(ArrayList<Products> proorderList) {
        this.proorderList = proorderList;
    }

    public void setAddproducts(Products addproducts) {
        this.addproducts = addproducts;
    }

    public ArrayList<Category> getSliderList() {
        return sliderList;
    }

    public Boolean getInitDone() {
        return isInitDone;
    }

    public void setInitDone(Boolean initDone) {
        isInitDone = initDone;
    }

    public void setSliderList(ArrayList<Category> sliderList) {
        this.sliderList = sliderList;
    }

    public void setCategoryList(ArrayList<Category> categoryList) {
        this.categoryList = categoryList;

    }

//    public ArrayList<Plan> getPlanArrayList() {
//        return planArrayList;
//    }
//
//    public void setPlanArrayList(ArrayList<Plan> planArrayList) {
//        this.planArrayList = planArrayList;
//    }

    public ArrayList<Tables> getOrderHistorylist() {
        return orderHistorylist;
    }

    public void setOrderHistorylist(ArrayList<Tables> orderHistorylist) {
        this.orderHistorylist = orderHistorylist;
    }

    public ArrayList<Products> getCartItems() {
        return cartItems;
    }

    public void setCartItems(ArrayList<Products> cartItems) {
        this.cartItems = cartItems;
    }

    public Integer getCartqunatity() {
        return cartqunatity;
    }

    public void setCartqunatity(Integer cartqunatity) {
        this.cartqunatity = cartqunatity;
    }

    public HashMap<Integer, ArrayList<Category>> getSubcatMapping() {
        return subcatMapping;
    }

    public void setSubcatMapping(HashMap<Integer, ArrayList<Category>> subcatMapping) {
        this.subcatMapping = subcatMapping;
    }

    public ArrayList<Tables> getTablelist() {
        return tablelist;
    }

    public void setTablelist(ArrayList<Tables> tablelist) {
        this.tablelist = tablelist;
    }

    public ArrayList<Products> getmProductList() {
        return mProductList;
    }

    public void setmProductList(ArrayList<Products> mProductList) {
        this.mProductList = mProductList;
    }

    public ArrayList<Category> getProductList2() {
        return ProductList2;
    }

    public void setProductList2(ArrayList<Category> productList2) {
        ProductList2 = productList2;
    }

    //    void dummyData() {
//
//        ArrayList<Category> listcoffe = new ArrayList<Category>();
//        for (int i = 0; i < 15; i++) {
//            Category prd = new Category();
//            prd.setCatImage("https://www.archibaldsburgers.com/wp-content/uploads/2016/07/veggies.png");
//            prd.setCatName("Vegetables");
//            listcoffe.add(prd);
//            categoryList.add(prd);
//        }
//
//        ArrayList<Products> products = new ArrayList<Products>();
//        for (int i = 0; i < 15; i++) {
//            Products prd = new Products();
//            prd.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRqpMXqByx7zuR182SGmikC_nEBGxw9ILqErQkSRFQHC-VB4XVU");
//            prd.setTitle("Beetroot");
//            prd.setPrice("200");
//            products.add(prd);
//            mProductList.add(prd);
//        }
//
//        ArrayList<Address> address = new ArrayList<Address>();
//        for (int i = 0; i < 3; i++) {
//            Address prd = new Address();
//            prd.setCity("Bangalore");
//            prd.setAddress("Vidyaranyapura");
//            prd.setCountry("India");
//            prd.setLandmark("Vidyaranyapura");
//            prd.setPincode("567123");
//            address.add(prd);
//            addresslist.add(prd);
//        }
//
//    }
}

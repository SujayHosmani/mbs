package com.MakemyBeauty.makemybeauty.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.MakemyBeauty.makemybeauty.Interface.Ichild;
import com.MakemyBeauty.makemybeauty.Model.SubServiceModel;
import com.MakemyBeauty.makemybeauty.Model.ValidateOtpModel;
import com.MakemyBeauty.makemybeauty.Model.ViewCartModel;
import com.MakemyBeauty.makemybeauty.Network.NetworkClass;
import com.MakemyBeauty.makemybeauty.Network.VolleyCallback;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Utilities.AppConstant;
import com.android.volley.Request;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;
import static com.MakemyBeauty.makemybeauty.Adapter.ViewCartAdapter.*;

public class ViewCartAdapter extends RecyclerView.Adapter<ViewCartAdapter.ViewHolder> {
    Context context;
    private LayoutInflater inflater;
    int vibrateVal = 18;
    SharedPreferences prefs,pref2;
    SharedPreferences.Editor editor;
    Vibrator vibrate;
    ArrayList<ViewCartModel> al;
    private Ichild mChildlistner;
    String TAG = "ViewCartAdapter";

    public  ViewCartAdapter(Context context, ArrayList<ViewCartModel> al, Ichild listener){
        this.al = al;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.vibrate = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);
        this.prefs = this.context.getSharedPreferences("menu", MODE_PRIVATE);
        this.pref2 = this.context.getSharedPreferences("profile", MODE_PRIVATE);
        this.editor = this.prefs.edit();
        this.mChildlistner = listener;

    }
    @NonNull
    @Override
    public ViewCartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.view_cart, parent, false);
        ViewCartAdapter.ViewHolder vh = new ViewCartAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewCartAdapter.ViewHolder holder, final int position) {
        final ViewCartModel model = al.get(position);
        holder.mainTxt.setText(model.getS_name());
        holder.subTxt.setText("₹" + model.getS_price());
        if (model.getStatus().equals("pending")){
            holder.mStatus.setText("Pending");
            holder.mStatus.setTextColor(0xffEB7E30);
            holder.mMinus.setEnabled(true);
            holder.mCallBtn.setEnabled(false);
            holder.mPlus.setEnabled(true);
            holder.mCallBtn.setBackgroundColor(holder.mCallBtn.getContext().getResources().getColor(R.color.grayz));
        }else if(model.getStatus().equals("approved")){
            holder.mMinus.setEnabled(false);
            holder.mPlus.setEnabled(false);
            holder.mStatus.setText(model.getStatus());
            holder.mStatus.setTextColor(0xff60b245);
            holder.mCallBtn.setBackgroundColor(0x9960b245);
            holder.mCallBtn.setEnabled(true);
        }

        if (model.getVendor_name().isEmpty()){
            holder.mVendorLayout.setVisibility(View.GONE);
        }else {
            holder.mVendorLayout.setVisibility(View.VISIBLE);
            holder.mVendorName.setText(model.getVendor_name());
        }


        if (model.getC_id().equals("1")) {
            if (model.getS_name().toLowerCase().contains("hair")) {
                holder.mainImg.setActualImageResource(R.drawable.hairmen);
            } else if (model.getS_name().toLowerCase().contains("massage")) {
                holder.mainImg.setActualImageResource(R.drawable.massagemen);
            } else if (model.getS_name().toLowerCase().contains("skin")) {
                holder.mainImg.setActualImageResource(R.drawable.skinmen);
            } else {
                holder.mainImg.setActualImageResource(R.drawable.packagefemale);
            }
        } else if (model.getC_id().equals("2")) {
            if (model.getS_name().toLowerCase().contains("hair")) {
                holder.mainImg.setActualImageResource(R.drawable.hairfemale);
            } else if (model.getS_name().toLowerCase().contains("massage")) {
                holder.mainImg.setActualImageResource(R.drawable.massagefemale);
            } else if (model.getS_name().toLowerCase().contains("skin")) {
                holder.mainImg.setActualImageResource(R.drawable.skinfemale);
            } else {
                holder.mainImg.setActualImageResource(R.drawable.packagefemale);
            }
        }else{
            if (model.getS_name().toLowerCase().contains("hair")) {
                holder.mainImg.setActualImageResource(R.drawable.hairfemale);
            } else if (model.getS_name().toLowerCase().contains("massage")) {
                holder.mainImg.setActualImageResource(R.drawable.massagefemale);
            } else if (model.getS_name().toLowerCase().contains("skin")) {
                holder.mainImg.setActualImageResource(R.drawable.skinfemale);
            } else {
                holder.mainImg.setActualImageResource(R.drawable.packagefemale);
            }
        }

        int val = Integer.parseInt(model.getPro_qnty());
        if (val > 0){
            holder.mAdd.setText(""+val);
            holder.mCount = val;
            holder.mMinus.setVisibility(View.VISIBLE);
            holder.mPlus.setVisibility(View.VISIBLE);
        }else{
            holder.mMinus.setVisibility(View.GONE);
            holder.mPlus.setVisibility(View.GONE);
            holder.mCount = 0;
            holder.mAdd.setText("ADD");
        }



        holder.mPlus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate.vibrate(vibrateVal);
                if (holder.mPlus.getVisibility() == View.VISIBLE){
                    holder.mCount = holder.mCount + 1;
                    insertValues(model,holder.mCount, position, holder, "plus");
                    goUpSide(holder.mAdd, holder.mCount - 1, holder.mCount);
                }
            }
        });



        holder.mMinus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate.vibrate(vibrateVal);
                if (holder.mMinus.getVisibility() == View.VISIBLE){
                    holder.mCount = holder.mCount - 1;
                    if (holder.mCount == 0){
                        deleteFromCart(model,position,holder);
                    }else{
                        insertValues(model,holder.mCount, position, holder, "minus");
                        goDown(holder.mAdd, holder.mCount);
                    }
                }
            }
        });

        holder.mDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteFromCart(model,position,holder);
            }
        });


        holder.mCallBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mChildlistner.onChildClick(model.getVendor_ph(),position,model);
            }
        });



    }

    private void updateCart(final int count, ViewCartModel model, final int position, final ViewHolder holder, final String from){
        if (count == 0){
            deleteFromCart(model, position, holder);
        }else{
            final String mobile = pref2.getString("mobile", "");
            final String token = pref2.getString("token", "");

            HashMap<String, String> Params = new HashMap<String, String>();
            Params.put("mobile", mobile);
            Params.put("token", token);
            Params.put("pro_qnty", String.valueOf(count));
            Params.put("cart_id", model.getCart_id());


            Log.d(TAG, "The cart params = "+ Params);
            NetworkClass.MakeRequest(AppConstant.url_base + "updateCart", Params, Request.Method.POST,   context, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject result) throws JSONException {
                    Log.d(TAG,"the otp response: " + result);

                    Gson gson= new Gson();
                    ValidateOtpModel errModel = gson.fromJson(result.toString(), ValidateOtpModel.class);
                    if (errModel.getErrorCode() == 200){
                        //Toast.makeText(context, "Appointment booked successfully", Toast.LENGTH_SHORT).show();
                        al.get(position).setPro_qnty(String.valueOf(count));

                    }else{
                        Toast.makeText(context, "" + errModel.getMessage(), Toast.LENGTH_SHORT).show();
                        if (from.equals("plus")){
                            holder.mCount = holder.mCount - 1;
                            holder.mAdd.setText(""+holder.mCount);
                        }else{
                            holder.mCount = holder.mCount + 1;
                            holder.mAdd.setText(""+holder.mCount);
                        }
                    }

                }

                @Override
                public void onError(String result) throws Exception {
                    Log.d(TAG,"the otp Error: " + result);
                    Toast.makeText(context, "Error: " + result, Toast.LENGTH_SHORT).show();
                    if (from.equals("plus")){
                        holder.mCount = holder.mCount - 1;
                        holder.mAdd.setText(""+holder.mCount);
                    }else{
                        holder.mCount = holder.mCount + 1;
                        holder.mAdd.setText(""+holder.mCount);
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }

    private void deleteFromCart(ViewCartModel model, final int position, final ViewHolder holder) {
        final String mobile = pref2.getString("mobile", "");
        final String token = pref2.getString("token", "");

        HashMap<String, String> Params = new HashMap<String, String>();
        Params.put("mobile", mobile);
        Params.put("token", token);
        Params.put("cart_id", model.getCart_id());


        Log.d(TAG, "The cart params = "+ Params);
        NetworkClass.MakeRequest(AppConstant.url_base + "deleteCart", Params, Request.Method.POST,   context, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) throws JSONException {
                Log.d(TAG,"the otp response: " + result);

                Gson gson= new Gson();
                ValidateOtpModel errModel = gson.fromJson(result.toString(), ValidateOtpModel.class);
                if (errModel.getErrorCode() == 200){
                    //Toast.makeText(context, "Appointment booked successfully", Toast.LENGTH_SHORT).show();
                    al.remove(position);
                    notifyDataSetChanged();
                }else{
                    Toast.makeText(context, "" + errModel.getMessage(), Toast.LENGTH_SHORT).show();

                        holder.mCount = holder.mCount + 1;
                        holder.mAdd.setText(""+holder.mCount);

                }

            }

            @Override
            public void onError(String result) throws Exception {
                Log.d(TAG,"the otp Error: " + result);
                Toast.makeText(context, "Error: " + result, Toast.LENGTH_SHORT).show();
                holder.mCount = holder.mCount + 1;
                holder.mAdd.setText(""+holder.mCount);
                notifyDataSetChanged();
            }
        });
    }


    private int checkForValuesInserted(TextView mAdd, ViewCartModel model) {
        int name = prefs.getInt(model.getS_id(), 0);
        return name;
    }


    private void goUpSide(final TextView textView, final int currentCount, final int nextCount) {
        Animation slide_up = AnimationUtils.loadAnimation(context,
                R.anim.slide_up);
        textView.setText(""+currentCount);
        slide_up.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                bringFromDown(textView,""+nextCount);

            }
        });
        textView.startAnimation(slide_up);
    }


    private void insertValues(ViewCartModel model, int count, int position, ViewHolder holder, String from){
        updateCart(count,model, position, holder, from);

    }

    private void bringFromDown(final TextView textView, String value) {
        Animation slide_bringFromDown = AnimationUtils.loadAnimation(context,
                R.anim.bring_from_down);

        textView.setText(value);

        textView.startAnimation(slide_bringFromDown);
    }


    private void goDown(final TextView textView, final int nextCount) {
        Animation slide_down = AnimationUtils.loadAnimation(context,
                R.anim.slide_down);

        slide_down.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                bringFromUp(textView,""+nextCount);

            }
        });

        textView.startAnimation(slide_down);
    }

    public void bringFromUp(final TextView textView, final String nextCount){
        Animation slide_bringFromDown = AnimationUtils.loadAnimation(context,
                R.anim.bring_from_up);

        textView.setText(nextCount);

        textView.startAnimation(slide_bringFromDown);
    }

    @Override
    public int getItemCount() {
        return al.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView mainImg;
        TextView mainTxt, subTxt,mAdd, mStatus;
        int mCount = 0;
        ImageView mPlus, mMinus;
        LinearLayout mPlus2, mMinus2;
        ConstraintLayout mConstraintLayout,mVendorLayout;
        TextView mVendorName;
        Button mAddBtn, mDeleteBtn, mCallBtn;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mainTxt = itemView.findViewById(R.id.txtServiceName);
            subTxt = itemView.findViewById(R.id.rupee);
            mainImg = itemView.findViewById(R.id.imgService);
            mAddBtn = itemView.findViewById(R.id.btnAdd);
            mDeleteBtn = itemView.findViewById(R.id.delete_btn);
            mCallBtn = itemView.findViewById(R.id.call_btn);
            mVendorLayout = itemView.findViewById(R.id.payment);
            mVendorName = itemView.findViewById(R.id.payment_value);
            mAdd = itemView.findViewById(R.id.addQ);
            mPlus = itemView.findViewById(R.id.plus);
            mMinus = itemView.findViewById(R.id.minus);
            mPlus2 = itemView.findViewById(R.id.plus2);
            mMinus2 = itemView.findViewById(R.id.minus2);
            mStatus = itemView.findViewById(R.id.status_value);
            mConstraintLayout = itemView.findViewById(R.id.addView);

        }
    }
}


package com.MakemyBeauty.makemybeauty.Adapter;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.MakemyBeauty.makemybeauty.Activities.ServicesActivity;
import com.MakemyBeauty.makemybeauty.Model.Category;
import com.MakemyBeauty.makemybeauty.Model.Products;
import com.MakemyBeauty.makemybeauty.Model.Service;
import com.MakemyBeauty.makemybeauty.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<Category> mcatList;
    LayoutInflater mInflater;
    //   private Integer viewingPage;

//    public interface MyAdapterListener {
//        void onPlusButtonClick(View v, int position);
//        void onMinusButtonClick(View v, int position);
//        void onAddToCartButtonClick(View v, int position);
//
//    }
//
//    static  public MyAdapterListener onClickListener2 ;


    public interface OnItemClickListener {
        void onItemClick(View view, int position);

    }


    static  public ServiceAdapter.OnItemClickListener onClickListener;

    public ServiceAdapter(Context c, ArrayList<Category> catList, OnItemClickListener listener) {
        this.mcatList = catList;
        this.mContext = c;
//        onClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.servicelist_card, parent, false);

        return new ServiceAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        Category cat=mcatList.get(position);
        holder.textView.setText(mcatList.get(position).getServiceName());
       // holder.textView.setText(mcatList.get(position).getServiceId());
        holder.product_pricetxt.setText(mcatList.get(position).getPrice());
        holder.product_pricetxt.setTag(position);
        holder.textView.setTag(position);
        holder.imageView.setTag(position);


        //Todo: add to cart start
        holder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String productName = mcatList.get(position).getServiceName().toString();
                Toast.makeText(mContext, productName + position, Toast.LENGTH_SHORT).show();

                Intent intent=new Intent(mContext, ServicesActivity.class);

                intent.putExtra("s_id",mcatList.get(position).getServiceId());
                intent.putExtra("s_name",mcatList.get(position).getServiceName());
                intent.putExtra("pro_qnty",String.valueOf(mcatList.get(position).getQuantity()));
                intent.putExtra("s_price",mcatList.get(position).getPrice());
                intent.putExtra("s_image",mcatList.get(position).getServiceImage());

                mContext.startActivity(intent);
            }
        });

//        holder.productName.setTag(position);
//        holder.productDetails.setTag(position);
//        holder.mrp.setTag(position);
//        holder.image1.setTag(position);

//        holder.image1.setTag(position);
        //Todo: add to cart end

        try {
            //System.out.println("jyo main image null"+cat.getCatImage());
            Picasso.with(mContext).load(cat.getPro_image()).into(holder.imageView);
        } catch (Exception e) {

        }

//        holder.textView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onClickListener.onItemClick(v,position);
//            }
//        });
//
//        holder.imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onClickListener.onItemClick(v, position);
//
//
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return mcatList == null ? 0: mcatList.size();
    }

    public void setData(ArrayList<Category>catList) {
        mcatList =  catList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView textView,product_pricetxt;
        public CardView card_all_product;
        public Button btnAdd;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            card_all_product = (CardView) itemView.findViewById(R.id.CDaddCart);

            textView = (TextView) itemView.findViewById(R.id.txtServiceName);
            product_pricetxt =itemView.findViewById(R.id.txtServicePrice);
            imageView = (ImageView)itemView.findViewById(R.id.imgService);
            btnAdd = (Button)itemView.findViewById(R.id.btnAdd);




        }
    }

}

package com.MakemyBeauty.makemybeauty.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.MakemyBeauty.makemybeauty.Activities.HomeActivity;
import com.MakemyBeauty.makemybeauty.Model.Category;
import com.MakemyBeauty.makemybeauty.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Category> categoryList;
    LayoutInflater mInflater;

    static  public OnItemClickListener onClickListener;


    public CategoryAdapter(HomeActivity context, ArrayList<Category> categoryList, OnItemClickListener onItemClickListener) {
        this.categoryList = this.categoryList;
        this.mContext = context;
        onClickListener = (OnItemClickListener) this.categoryList;

    }


    public interface OnItemClickListener {

        void onItemClick(View view, int position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        Category cat=categoryList.get(position);
        holder.textView.setText(categoryList.get(position).getSubcatName());

        holder.textView.setTag(position);
        holder.imageView.setTag(position);
        try {
            //System.out.println("jyo main image null"+cat.getCatImage());
            Picasso.with(mContext).load(cat.getCatImage()).into(holder.imageView);
        } catch (Exception e) {

        }

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemClick(v,position);
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemClick(v, position);
            }
        });

    }




    @Override
    public int getItemCount() {
        return categoryList == null ? 0: categoryList.size();
    }
    public void setData(ArrayList<Category>categories) {
        categoryList =  categories;
        notifyDataSetChanged();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        CardView cardView;



        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardnew);

            textView = (TextView) itemView.findViewById(R.id.textViewnew);
            imageView = (ImageView)itemView.findViewById(R.id.imageViewnew);

        }
    }
}


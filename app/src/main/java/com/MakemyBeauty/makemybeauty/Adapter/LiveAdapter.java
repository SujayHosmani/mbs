package com.MakemyBeauty.makemybeauty.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.MakemyBeauty.makemybeauty.Activities.SubServicesActivity;
import com.MakemyBeauty.makemybeauty.Interface.Ichild;
import com.MakemyBeauty.makemybeauty.Model.Category;
import com.MakemyBeauty.makemybeauty.Model.ProfileModel;
import com.MakemyBeauty.makemybeauty.Model.SubCatModel;
import com.MakemyBeauty.makemybeauty.R;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;

public class LiveAdapter extends RecyclerView.Adapter<LiveAdapter.ViewHolder> {
    Context context;
    private LayoutInflater inflater;
    ArrayList<SubCatModel> al;

    public  LiveAdapter(Context context, ArrayList<SubCatModel> al){
        this.al = al;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }
    @NonNull
    @Override
    public LiveAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.global_item, parent, false);
        LiveAdapter.ViewHolder vh = new LiveAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull LiveAdapter.ViewHolder holder, int position) {
        final SubCatModel v = al.get(position);
        if (v.getCategory().equals("1")){
            if (v.getSubcategory().contains("Hair")){
                holder.mainImg.setActualImageResource(R.drawable.hairmen);
            }else if (v.getSubcategory().contains("Massage")){
                holder.mainImg.setActualImageResource(R.drawable.massagemen);
            }else if (v.getSubcategory().contains("Skin")){
                holder.mainImg.setActualImageResource(R.drawable.skinmen);
            }else if (v.getSubcategory().contains("Packages")){
                holder.mainImg.setActualImageResource(R.drawable.packagesmen);
            }
        }else if (v.getCategory().equals("2")){
            if (v.getSubcategory().contains("Hair")){
                holder.mainImg.setActualImageResource(R.drawable.hairfemale);
            }else if (v.getSubcategory().contains("Massage")){
                holder.mainImg.setActualImageResource(R.drawable.massagefemale);
            }else if (v.getSubcategory().contains("Skin")){
                holder.mainImg.setActualImageResource(R.drawable.skinfemale);
            }else if (v.getSubcategory().contains("Packages")){
                holder.mainImg.setActualImageResource(R.drawable.packagefemale);
            }
        }
        holder.name.setText(v.getSubcategory());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SubServicesActivity.class);
                intent.putExtra("subcat",v.getSubcategory_id());
                intent.putExtra("cat",v.getCategory());
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return al.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView mainImg;
        TextView name, sub;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mainImg = itemView.findViewById(R.id.fac_img);
            name = itemView.findViewById(R.id.fac_name);
            sub = itemView.findViewById(R.id.fac_sub);
        }
    }
}

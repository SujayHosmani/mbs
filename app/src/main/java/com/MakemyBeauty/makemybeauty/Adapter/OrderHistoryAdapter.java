package com.MakemyBeauty.makemybeauty.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.MakemyBeauty.makemybeauty.Model.Order;
import com.MakemyBeauty.makemybeauty.Model.Tables;
import com.MakemyBeauty.makemybeauty.R;

import java.util.ArrayList;

public class OrderHistoryAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Tables> list;
    LayoutInflater layoutInflater = null;

    public OrderHistoryAdapter(Context context, ArrayList<Tables> list) {
        this.context = context;
        this.list = list;
        this.layoutInflater = LayoutInflater.from(context);

    }

    public class Holder {
        TextView tv_orderID,tv_orderDte,tv_amt,tv_status,tv_delvDte;
        CardView cardview_order_history;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = layoutInflater.inflate(R.layout.activity_my_order,null);

        Holder holder = new Holder();

        holder.cardview_order_history = view.findViewById(R.id.cardview_order_history);

        holder.tv_orderID = view.findViewById(R.id.tv_orderID);
        holder.tv_orderDte = view.findViewById(R.id.tv_orderDte);
        holder.tv_amt = view.findViewById(R.id.tv_amt);
//        holder.tv_status = view.findViewById(R.id.tv_status);
        holder.tv_delvDte = view.findViewById(R.id.tv_delvDte);

        final Tables model = list.get(position);

        holder.tv_orderID.setText("Order Id:  "+list.get(position).getOrder_id());
        holder.tv_orderDte.setText("Order Time:  "+list.get(position).getCreated_on());
        holder.tv_amt.setText("Amount:  "+list.get(position).getGrand_price());
        // list.get(position).getOrder_status()==0
        ///holder.tv_status.setText("Status:  "+list.get(position).getOrder_status());//Html.fromHtml("Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<font color='green'>"+model.getTitle()+"</font>"));

        return view;
    }
}

package com.MakemyBeauty.makemybeauty.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.MakemyBeauty.makemybeauty.Model.GlobalApiModel;
import com.MakemyBeauty.makemybeauty.Model.SubCatModel;
import com.MakemyBeauty.makemybeauty.Model.SubServiceModel;
import com.MakemyBeauty.makemybeauty.Model.ValidateOtpModel;
import com.MakemyBeauty.makemybeauty.Network.NetworkClass;
import com.MakemyBeauty.makemybeauty.Network.VolleyCallback;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Utilities.AppConstant;
import com.android.volley.Request;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;

public class SubLiveAdapter extends RecyclerView.Adapter<SubLiveAdapter.ViewHolder> {
    Context context;
    private LayoutInflater inflater;
    int vibrateVal = 18;
    SharedPreferences prefs,pref2;
    SharedPreferences.Editor editor;
    Vibrator vibrate;
    ArrayList<SubServiceModel> al;
    String TAG = "SubLiveAdapter";

    public  SubLiveAdapter(Context context, ArrayList<SubServiceModel> al){
        this.al = al;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.vibrate = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);
        this.prefs = this.context.getSharedPreferences("menu", MODE_PRIVATE);
        this.pref2 = this.context.getSharedPreferences("profile", MODE_PRIVATE);
        this.editor = this.prefs.edit();

    }
    @NonNull
    @Override
    public SubLiveAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.servicelist_card, parent, false);
        SubLiveAdapter.ViewHolder vh = new SubLiveAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final SubLiveAdapter.ViewHolder holder, final int position) {
        final SubServiceModel model = al.get(position);
        holder.mainTxt.setText(model.getServices());
        holder.subTxt.setText("₹" + model.getPrice());


        if (model.getCategory().equals("1")) {
            if (model.getServices().toLowerCase().contains("hair")) {
                holder.mainImg.setActualImageResource(R.drawable.hairmen);
            } else if (model.getServices().toLowerCase().contains("massage")) {
                holder.mainImg.setActualImageResource(R.drawable.massagemen);
            } else if (model.getServices().toLowerCase().contains("skin")) {
                holder.mainImg.setActualImageResource(R.drawable.skinmen);
            } else {
                holder.mainImg.setActualImageResource(R.drawable.packagefemale);
            }
        } else if (model.getCategory().equals("2")) {
            if (model.getServices().toLowerCase().contains("hair")) {
                holder.mainImg.setActualImageResource(R.drawable.hairfemale);
            } else if (model.getServices().toLowerCase().contains("massage")) {
                holder.mainImg.setActualImageResource(R.drawable.massagefemale);
            } else if (model.getServices().toLowerCase().contains("skin")) {
                holder.mainImg.setActualImageResource(R.drawable.skinfemale);
            } else {
                holder.mainImg.setActualImageResource(R.drawable.packagefemale);
            }
        }

        int val = checkForValuesInserted(holder.mAdd,model);
        if (val > 0){
            holder.mAdd.setText(""+val);
            holder.mCount = val;
            holder.mMinus.setVisibility(View.VISIBLE);
            holder.mPlus.setVisibility(View.VISIBLE);
        }else{
            holder.mMinus.setVisibility(View.GONE);
            holder.mPlus.setVisibility(View.GONE);
            holder.mCount = 0;
            holder.mAdd.setText("ADD");
        }

        holder.mConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.mMinus.getVisibility() == View.GONE){
                    vibrate.vibrate(vibrateVal);
                    holder.mMinus.setVisibility(View.VISIBLE);
                    holder.mPlus.setVisibility(View.VISIBLE);
                    holder.mCount = holder.mCount + 1;
                    insertValues(model,holder.mCount);
                    holder.mAdd.setText(""+holder.mCount);
                }
            }
        });

        holder.mPlus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate.vibrate(vibrateVal);
                if (holder.mPlus.getVisibility() == View.VISIBLE){
                    holder.mCount = holder.mCount + 1;
                    insertValues(model,holder.mCount);
                    goUpSide(holder.mAdd, holder.mCount - 1, holder.mCount);
                }else{
                    holder.mMinus.setVisibility(View.VISIBLE);
                    holder.mPlus.setVisibility(View.VISIBLE);
                    holder.mCount = holder.mCount + 1;
                    insertValues(model,holder.mCount);
                    holder.mAdd.setText(""+holder.mCount);
                }
            }
        });

        holder.mAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToCart(model);
            }
        });

        holder.mMinus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate.vibrate(vibrateVal);
                if (holder.mMinus.getVisibility() == View.VISIBLE){
                    holder.mCount = holder.mCount - 1;
                    if (holder.mCount == 0){
                        holder.mMinus.setVisibility(View.GONE);
                        holder.mPlus.setVisibility(View.GONE);
                        holder.mAdd.setText("ADD");
                        insertValues(model,holder.mCount);
                    }else{
                        insertValues(model,holder.mCount);
                        goDown(holder.mAdd, holder.mCount);
                    }
                }else{
                    holder.mMinus.setVisibility(View.VISIBLE);
                    holder.mPlus.setVisibility(View.VISIBLE);
                    holder.mCount = holder.mCount + 1;
                    insertValues(model,holder.mCount);
                    holder.mAdd.setText(""+holder.mCount);
                }
            }
        });



    }

    private void addToCart(SubServiceModel model){
        final String mobile = pref2.getString("mobile", "");
        final String token = pref2.getString("token", "");
        final String userId = pref2.getString("userid", "");
        final String userName = pref2.getString("name", "");

        String city = pref2.getString("city", "");
        String address = pref2.getString("address", "");
        String longitude = pref2.getString("longitude", "");
        String latitude = pref2.getString("latitude", "");

        HashMap<String, String> Params = new HashMap<String, String>();
        Params.put("mobile", mobile);
        Params.put("token", token);
        Params.put("user_id", userId);
        Params.put("s_id", model.getService_id());
        Params.put("subcategory_id", model.getSubcategory());
        Params.put("s_name", model.getServices());
        Params.put("pro_qnty", "1");
        Params.put("s_price", model.getPrice());
        Params.put("latitude", latitude);
        Params.put("longitude", longitude);
        Params.put("city", city);
        Params.put("address", address);
        Params.put("s_image", "");
        Params.put("user_name", userName);
        Params.put("c_id", model.getCategory());

        Log.d(TAG, "The cart params = "+ Params);
        NetworkClass.MakeRequest(AppConstant.url_base + "addCart", Params, Request.Method.POST,   context, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) throws JSONException {
                Log.d(TAG,"the otp response: " + result);

                Gson gson= new Gson();
                ValidateOtpModel errModel = gson.fromJson(result.toString(), ValidateOtpModel.class);
                if (errModel.getErrorCode() == 200){
                    Toast.makeText(context, "Appointment booked successfully", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(context, "" + errModel.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onError(String result) throws Exception {
                Log.d(TAG,"the otp Error: " + result);
                Toast.makeText(context, "Error: " + result, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private int checkForValuesInserted(TextView mAdd, SubServiceModel model) {
        int name = prefs.getInt(model.getService_id(), 0);
        return name;
    }


    private void goUpSide(final TextView textView, final int currentCount, final int nextCount) {
        Animation slide_up = AnimationUtils.loadAnimation(context,
                R.anim.slide_up);
        textView.setText(""+currentCount);
        slide_up.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                bringFromDown(textView,""+nextCount);

            }
        });
        textView.startAnimation(slide_up);
    }


    private void insertValues(SubServiceModel model, int count){
        editor.putInt(model.getService_id(), count);
        editor.apply();

    }

    private void bringFromDown(final TextView textView, String value) {
        Animation slide_bringFromDown = AnimationUtils.loadAnimation(context,
                R.anim.bring_from_down);

        textView.setText(value);

        textView.startAnimation(slide_bringFromDown);
    }


    private void goDown(final TextView textView, final int nextCount) {
        Animation slide_down = AnimationUtils.loadAnimation(context,
                R.anim.slide_down);

        slide_down.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                bringFromUp(textView,""+nextCount);

            }
        });

        textView.startAnimation(slide_down);
    }

    public void bringFromUp(final TextView textView, final String nextCount){
        Animation slide_bringFromDown = AnimationUtils.loadAnimation(context,
                R.anim.bring_from_up);

        textView.setText(nextCount);

        textView.startAnimation(slide_bringFromDown);
    }

    @Override
    public int getItemCount() {
        return al.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView mainImg;
        TextView mainTxt, subTxt,mAdd;
        int mCount = 0;
        ImageView mPlus, mMinus;
        LinearLayout mPlus2, mMinus2;
        ConstraintLayout mConstraintLayout;
        Button mAddBtn;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mainTxt = itemView.findViewById(R.id.txtServiceName);
            subTxt = itemView.findViewById(R.id.rupee);
            mainImg = itemView.findViewById(R.id.imgService);
            mAddBtn = itemView.findViewById(R.id.btnAdd);

            mAdd = itemView.findViewById(R.id.addQ);
            mPlus = itemView.findViewById(R.id.plus);
            mMinus = itemView.findViewById(R.id.minus);
            mPlus2 = itemView.findViewById(R.id.plus2);
            mMinus2 = itemView.findViewById(R.id.minus2);

            mConstraintLayout = itemView.findViewById(R.id.addView);

        }
    }
}

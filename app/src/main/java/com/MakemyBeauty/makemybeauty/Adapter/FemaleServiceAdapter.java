package com.MakemyBeauty.makemybeauty.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.MakemyBeauty.makemybeauty.Model.Category;
import com.MakemyBeauty.makemybeauty.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FemaleServiceAdapter extends RecyclerView.Adapter<FemaleServiceAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Category> mcatList;
    LayoutInflater mInflater;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    static  public FemaleServiceAdapter.OnItemClickListener onClickListener;

    public FemaleServiceAdapter(Context c, ArrayList<Category> catList, OnItemClickListener listener) {
        this.mcatList = catList;
        this.mContext = c;
        onClickListener = listener;
    }


    @NonNull
    @Override
    public FemaleServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.female_servicelist_card, parent, false);


        return new FemaleServiceAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Category cat=mcatList.get(position);
        holder.textView.setText(mcatList.get(position).getServiceName());
        holder.product_pricetxt.setText(mcatList.get(position).getPrice());
        holder.product_pricetxt.setTag(position);
        holder.textView.setTag(position);
        holder.imageView.setTag(position);

        try {
            //System.out.println("jyo main image null"+cat.getCatImage());
            Picasso.with(mContext).load(cat.getPro_image()).into(holder.imageView);
        } catch (Exception e) {

        }

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemClick(v,position);
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemClick(v, position);


            }
        });
    }


    @Override
    public int getItemCount() {
        return mcatList == null ? 0: mcatList.size();
    }

    public void setData(ArrayList<Category>catList) {
        mcatList =  catList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView textView,product_pricetxt;
        public CardView card_all_product;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            card_all_product = (CardView) itemView.findViewById(R.id.CDaddCart);

            textView = (TextView) itemView.findViewById(R.id.txtServiceName);
            product_pricetxt =itemView.findViewById(R.id.txtServicePrice);
            imageView = (ImageView)itemView.findViewById(R.id.imgService);


        }
    }
}

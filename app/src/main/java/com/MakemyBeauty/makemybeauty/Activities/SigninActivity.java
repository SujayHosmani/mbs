package com.MakemyBeauty.makemybeauty.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.MakemyBeauty.makemybeauty.Model.GlobalApiModel;
import com.MakemyBeauty.makemybeauty.Model.ProfileModel;
import com.MakemyBeauty.makemybeauty.Model.ValidateOtpModel;
import com.MakemyBeauty.makemybeauty.Network.NetworkClass;
import com.MakemyBeauty.makemybeauty.Network.VolleyCallback;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Utilities.AppConstant;
import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class SigninActivity extends AppCompatActivity {

    EditText mMobile,mPassword;
    Button mSubmit;
    TextView tvAlreadyAccountSignin;
    String TAG = "SigninActivity";
    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        mMobile = findViewById(R.id.mob_no);
        mPassword = findViewById(R.id.mob_pass);
        mSubmit = findViewById(R.id.sign_btn);
        tvAlreadyAccountSignin = findViewById(R.id.tvAlreadyAccountSignin);
        prefs = getSharedPreferences("profile", MODE_PRIVATE);

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMobile.getText().toString().isEmpty() && mPassword.getText().toString().isEmpty()){
                    if (mMobile.getText().toString().isEmpty()){
                        mMobile.setError("Enter mobile");
                    }
                    if (mPassword.getText().toString().isEmpty()){
                        mMobile.setError("Enter password");
                    }
                }else{
                    signIn();
                }
            }
        });

        tvAlreadyAccountSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SigninActivity.this, UserotpVerificationActivity.class);
                startActivity(intent);
            }
        });

    }


    private void signIn(){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("mobile", mMobile.getText().toString());
        params.put("password", mPassword.getText().toString());
        NetworkClass.MakeRequest(AppConstant.url_base + "login", params, Request.Method.POST,   SigninActivity.this, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) throws JSONException {
                Gson gson= new Gson();
                Type collectionType = new TypeToken<GlobalApiModel<ProfileModel>>(){}.getType();
                GlobalApiModel<ProfileModel> obj = gson.fromJson(result.toString(), collectionType);
                Toast.makeText(SigninActivity.this, "" + obj.getMessage(), Toast.LENGTH_SHORT).show();
                if (obj.getErrorCode() == 200){
                    ArrayList<ProfileModel> userArr = obj.getData();
                    ProfileModel pm = userArr.get(0);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("userid", pm.getUser_id());
                    editor.putString("name", pm.getName());
                    editor.putString("dob", pm.getDob());
                    editor.putString("email", pm.getEmail());
                    editor.putString("address", pm.getAddress());
                    editor.putString("city", pm.getCity());
                    editor.putString("gender", pm.getGender());
                    editor.putString("token", pm.getToken());
                    editor.putString("latitude", pm.getLatitude());
                    editor.putBoolean("isLogged", true);
                    editor.putString("mobile", pm.getMobile());
                    editor.putString("longitude", pm.getLongitude());
                    editor.apply();
                    Intent intent = new Intent(SigninActivity.this, LiveActivity.class);
                    finish();
                    startActivity(intent);
                }

            }

            @Override
            public void onError(String result) throws Exception {
                Toast.makeText(SigninActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
//package com.MakemyBeauty.makemybeauty.Activities;
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.Toast;
//
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.recyclerview.widget.GridLayoutManager;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.MakemyBeauty.makemybeauty.Adapter.FemaleServiceAdapter;
//import com.MakemyBeauty.makemybeauty.Adapter.ServiceAdapter;
//import com.MakemyBeauty.makemybeauty.Model.UC;
//import com.MakemyBeauty.makemybeauty.R;
//import com.MakemyBeauty.makemybeauty.Services.FetchFemaleServices;
//import com.MakemyBeauty.makemybeauty.Services.FetchServices;
//
//public class FemaleServicesActivity extends AppCompatActivity {
//
//    // Todo: Fetching Service (Based on Category and Subcategory)
//
//    //ListView listView;
//    RecyclerView view_product;
//    int id,id1=0;
//    // ScrollView scrollView;
//    FemaleServiceAdapter femaleServiceAdapter;
//    Button btnaddtocart;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_female_services);
//
//        netWorkAlert(this);
//
//        //Todo: commemted
//
//        view_product = (RecyclerView) findViewById(R.id.view_product);
//        view_product.setLayoutManager(new GridLayoutManager(getApplication(), 1));
//        int spanCount = 1; // 3 columns
//        int spacing = 1; // 50px
//        boolean includeEdge = false;
//
//        UC dg_inst = UC.getInstance();
//
//
//
//        Intent intent = getIntent();
//        id = intent.getIntExtra("id", 0);
//        id1 = intent.getIntExtra("subcat_id", 0);
//
//        FetchFemaleServices fetchServices = new FetchFemaleServices();
//
//        fetchServices.execute(id,id1);
//        try {
//
//            Toast.makeText(FemaleServicesActivity.this, "Fetchg service Data", Toast.LENGTH_SHORT).show();
//        } catch (Exception e) {
//
//            Toast.makeText(FemaleServicesActivity.this, "Application Fail to fetch Service data", Toast.LENGTH_SHORT).show();
//
//        }
//        LinearLayoutManager llm = new LinearLayoutManager(this);
//
//        femaleServiceAdapter = new FemaleServiceAdapter(FemaleServicesActivity.this, UC.getInstance().getProductList2(), new FemaleServiceAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//
//            }
//        });
//
//
//
//        view_product.setLayoutManager(llm);
//        view_product.setAdapter(femaleServiceAdapter);
//
//    }
//
//    public  static void netWorkAlert(final FemaleServicesActivity context){
//
//        if (isOnline(context)) {
//            //do whatever you want to do
//        } else {
//            try {
//                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//
//                alertDialog.setTitle("Info");
//                alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
//                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
//                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        context.finish();
//                    }
//                });
//
//                alertDialog.show();
//            } catch (Exception e) {
//                Log.e("Constants.TAG", "Show Dialog: " + e.getMessage());
//            }
//        }
//    }
//
//    private static boolean isOnline(Context context) {
//        ConnectivityManager conMgr = (ConnectivityManager)context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
//
//        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
//            // Toast.makeText(context, "No Internet connection!", Toast.LENGTH_LONG).show();
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    protected void onResume() {
//        UC ht_inst = UC.getInstance();
//        if(!ht_inst.getInitDone()) {
//            Intent intent = new Intent(FemaleServicesActivity.this, Spalsh_Activity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//        }
//        super.onResume();
//    }
//}

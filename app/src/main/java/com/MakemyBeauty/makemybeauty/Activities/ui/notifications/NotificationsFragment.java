package com.MakemyBeauty.makemybeauty.Activities.ui.notifications;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.MakemyBeauty.makemybeauty.Activities.LiveActivity;
import com.MakemyBeauty.makemybeauty.Activities.SigninActivity;
import com.MakemyBeauty.makemybeauty.Activities.UserRegistrationActivity;
import com.MakemyBeauty.makemybeauty.Activities.ViewCartActivity;
import com.MakemyBeauty.makemybeauty.Adapter.ViewCartAdapter;
import com.MakemyBeauty.makemybeauty.Interface.Ichild;
import com.MakemyBeauty.makemybeauty.Model.GlobalApiModel;
import com.MakemyBeauty.makemybeauty.Model.IvrModel;
import com.MakemyBeauty.makemybeauty.Model.ProfileModel;
import com.MakemyBeauty.makemybeauty.Model.ValidateOtpModel;
import com.MakemyBeauty.makemybeauty.Model.ViewCartModel;
import com.MakemyBeauty.makemybeauty.Network.NetworkClass;
import com.MakemyBeauty.makemybeauty.Network.VolleyCallback;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Utilities.AppConstant;
import com.android.volley.Request;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;


public class NotificationsFragment extends Fragment {

    RecyclerView mService;
    String TAG = "SubServicesActivity";
    ViewCartAdapter msubLiveAdapter;
    SharedPreferences prefs;
    ConstraintLayout mFadeView;
    private ProgressBar spinner;
    FusedLocationProviderClient fusedLocationProvideClient;
    ArrayList<ViewCartModel> serviceCategories;

    private NotificationsViewModel notificationsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);

        serviceCategories = new ArrayList<ViewCartModel>();
        mService = root.findViewById(R.id.re_cart);
        spinner = root.findViewById(R.id.progressBar1);
        mFadeView = root.findViewById(R.id.fade_view);

        spinner.setVisibility(View.GONE);
        mFadeView.setVisibility(View.GONE);

        LinearLayoutManager linearLayoutManagere = new LinearLayoutManager(getContext());
        linearLayoutManagere.setOrientation(LinearLayoutManager.VERTICAL);
        mService.setLayoutManager(linearLayoutManagere);
        prefs = getContext().getSharedPreferences("profile", MODE_PRIVATE);
        String mobile = prefs.getString("mobile", "");
        String token = prefs.getString("token", "");
        String userid = prefs.getString("userid", "");


        getServiceList(userid,mobile,token,"viewcart");
        fusedLocationProvideClient = LocationServices.getFusedLocationProviderClient(getActivity());
        getCurrentLocation();
       // getCallPermission();
        return root;
    }

    private boolean getCallPermission() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getContext(), "no permission", Toast.LENGTH_SHORT).show();
            requestPhonePermission();
            return false;
        }
        return true;

    }

    private void requestPhonePermission() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 2);
            return;
        }
    }

    private void getServiceList(String userid, final String mobile, String token, String subURL) {
        HashMap<String, String> maleSer = new HashMap<String, String>();
        maleSer.put("mobile", mobile);
        maleSer.put("token", token);
        maleSer.put("user_id", userid);
        NetworkClass.MakeRequest(AppConstant.url_base + subURL, maleSer, Request.Method.POST,   getContext(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) throws JSONException {
                Log.d(TAG,"the otp response: " + result);

                Gson gson= new Gson();
                ValidateOtpModel errModel = gson.fromJson(result.toString(), ValidateOtpModel.class);
                if (errModel.getErrorCode() == 200){
                    Type collectionType = new TypeToken<GlobalApiModel<ViewCartModel>>(){}.getType();
                    GlobalApiModel<ViewCartModel> obj = gson.fromJson(result.toString(), collectionType);
                    serviceCategories = obj.getData();
                    msubLiveAdapter = new ViewCartAdapter(getContext(), serviceCategories, new Ichild() {
                        @Override
                        public void onChildClick(String ph, int position, ViewCartModel vss) {
                            Log.d(TAG, ""+position);
                            connectCall(mobile, vss.getVendor_ph());

                        }
                    });
                    mService.setAdapter(msubLiveAdapter);
                }else{
                    Toast.makeText(getContext(), "" + errModel.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onError(String result) throws Exception {
                Log.d(TAG,"the otp Error: " + result);
                Toast.makeText(getContext(), "Error: " + result, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void connectCall(String user, String vendor){
        spinner.setVisibility(View.VISIBLE);
        mFadeView.setVisibility(View.VISIBLE);
//            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "8553655890"));
//            startActivity(intent);
        getTheToken(user, vendor);

    }

    private void getTheToken(final String user, final String vendor){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email","parnetstech@gmail.com");
        params.put("password", "123456");
        params.put("key", "a1640eaa57dss5748b0b240d03afe7f850b1447755404481ca8404voc2ff4fv43b434i746bl76l6e428j74637325e6i3v7il162sv4u3");
        NetworkClass.MakeRequest("http://ivr.aircamivr.in/api/authenticate", params, Request.Method.POST,   getContext(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) throws JSONException {
                Gson gson= new Gson();
                spinner.setVisibility(View.INVISIBLE);
                mFadeView.setVisibility(View.INVISIBLE);
                IvrModel obj = gson.fromJson(result.toString(), IvrModel.class);
                Toast.makeText(getContext(), "" + obj.getMessage(), Toast.LENGTH_SHORT).show();
                if (obj.getStatus().equals("success")){
                    createCall(obj.getToken(), user, vendor);
                }

            }

            @Override
            public void onError(String result) throws Exception {
                spinner.setVisibility(View.INVISIBLE);
                mFadeView.setVisibility(View.INVISIBLE);
                Toast.makeText(getContext(), "Error: " + result, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createCall(final String token, final String user, final String vendor) {
        spinner.setVisibility(View.VISIBLE);
        mFadeView.setVisibility(View.VISIBLE);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("token",token);
        params.put("did", "4398110");
        params.put("customer_number", user);
        params.put("executive_number", vendor);
        NetworkClass.MakeRequest("http://ivr.aircamivr.in/api/createCall", params, Request.Method.POST,   getContext(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) throws JSONException {
                Gson gson= new Gson();
                spinner.setVisibility(View.INVISIBLE);
                mFadeView.setVisibility(View.INVISIBLE);
                IvrModel obj = gson.fromJson(result.toString(), IvrModel.class);

                if (obj.getStatus().equals("success")){
                    Toast.makeText(getContext(), "Call created", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getContext(), "" + obj.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onError(String result) throws Exception {
                spinner.setVisibility(View.INVISIBLE);
                mFadeView.setVisibility(View.INVISIBLE);
                Toast.makeText(getContext(), "Error: " + result, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getContext(), "no permission", Toast.LENGTH_SHORT).show();
            requestLocationPermission();
            return;
        }
        fusedLocationProvideClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {

                Location location = task.getResult();

                if(location !=null){
                    //Toast.makeText(getContext(), "location is null", Toast.LENGTH_SHORT).show();

                    try {
                        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());

                        List<Address> addresses = geocoder.getFromLocation(
                                location.getLatitude(),location.getLongitude(),1
                        );
                        if (addresses.size() > 0){
                            boolean isLocationFetched = true;
                            double latitude = addresses.get(0).getLatitude();
                            double longitude = addresses.get(0).getLongitude();
                            String address = addresses.get(0).getAddressLine(0);
                            String city = addresses.get(0).getAdminArea();
                            //Toast.makeText(getContext(), ""+address, Toast.LENGTH_SHORT).show();

                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("address", address);
                            editor.putString("city", city);
                            editor.putString("latitude", String.valueOf(latitude));
                            editor.putString("longitude", String.valueOf(longitude));
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
    }

    private void requestLocationPermission() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    // User Allows Permission access
                    getCurrentLocation();

                } else {

                    // User denies Permission access

                }
                break;

        }
    }


}
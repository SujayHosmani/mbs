package com.MakemyBeauty.makemybeauty.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.MakemyBeauty.makemybeauty.R;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class LiveActivity extends AppCompatActivity {

    NavController navController;
    BottomNavigationView navView;
    BottomNavigationMenuView menuView;
    BottomNavigationItemView itemView;
    TextView textView;
    View notificationBadge;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live);
        navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications, R.id.navigation_menu)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        menuView = (BottomNavigationMenuView) navView.getChildAt(0);
        itemView = (BottomNavigationItemView) menuView.getChildAt(2);
        notificationBadge = LayoutInflater.from(this).inflate(R.layout.custom_badge_latout, menuView, false);
        textView = notificationBadge.findViewById(R.id.notifications_badge);

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        prefs = getSharedPreferences("menu", MODE_PRIVATE);


        //showBadge(2);// get count from api
    }

    @Override
    protected void onResume() {
        super.onResume();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("isPaid");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Boolean value = dataSnapshot.getValue(Boolean.class);
                Log.d("Firebase", "Value is: " + value);
                if (!value){
                    Intent intent = new Intent(LiveActivity.this, ProfileActivity.class);
                    finish();
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Firebase", "Failed to read value.", error.toException());
            }
        });
    }

    public void showBadge(int value){
        textView.setText(String.valueOf(value));
        itemView.addView(notificationBadge);
    }

    public void updateBadge(int value){
        textView.setText(String.valueOf(value));
    }

    public void removeBadge(){
        itemView.removeView(notificationBadge);
    }

}
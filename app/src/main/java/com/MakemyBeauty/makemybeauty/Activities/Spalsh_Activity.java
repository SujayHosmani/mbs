package com.MakemyBeauty.makemybeauty.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.MakemyBeauty.makemybeauty.Model.UC;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Utilities.Dao;
import com.MakemyBeauty.makemybeauty.Utilities.DbHelper;
import com.facebook.drawee.view.SimpleDraweeView;

public class Spalsh_Activity extends AppCompatActivity {

    Animation rotateAnimation;
    SimpleDraweeView imageView;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh_);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        DbHelper.createVendorDbHelper(this);
        Dao vendorDetailsDao = new Dao();
        vendorDetailsDao.getUserDetails();
        prefs = getSharedPreferences("profile", MODE_PRIVATE);
        imageView = findViewById(R.id.imageView);

        UC.getInstance().setInitDone(true);
        System.out.println("jyo get user id "+ UC.getInstance().getUser().getUserId());
       //System.out.println("jyo get vendor id "+ UC.getInstance().getUser().getVendorId());
        System.out.println("jyo get loginType "+ UC.getInstance().getUser().getLoginType());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                UC dg_inst = UC.getInstance();
                boolean isLogeedIn = prefs.getBoolean("isLogged", false);
                if (!isLogeedIn) {
                    Intent intent = new Intent(Spalsh_Activity.this,
                            SigninActivity.class);
                    startActivity(intent);
                    finish();

                }
                else {
                    Intent intent = new Intent(Spalsh_Activity.this,
                            LiveActivity.class);
                    startActivity(intent);
                    finish();
                }

                //todo: we just intent here we need change
//                Intent intent = new Intent(Spalsh_Activity.this, UserotpVerificationActivity.class);
//                startActivity(intent);
//                finish();
            }
        }, 3000);

        rotateAnimation();

        netWorkAlert(this);
    }

    private void rotateAnimation()
    {
        rotateAnimation= AnimationUtils.loadAnimation(this,R.anim.rotate);
        imageView.startAnimation(rotateAnimation);

    }

    private void netWorkAlert(final Spalsh_Activity context)
    {
        if (isOnline(context)) {

        } else {
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        context.finish();

                    }
                });

                alertDialog.show();
            } catch (Exception e) {
                Log.e("Constants.TAG", "Show Dialog: " + e.getMessage());
            }
        }
    }

    private static boolean isOnline(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager)context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            // Toast.makeText(context, "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

}

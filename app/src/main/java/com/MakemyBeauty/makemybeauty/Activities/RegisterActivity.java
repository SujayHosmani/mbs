package com.MakemyBeauty.makemybeauty.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.MakemyBeauty.makemybeauty.Model.UC;
import com.MakemyBeauty.makemybeauty.Network.NetworkClass;
import com.MakemyBeauty.makemybeauty.Network.VolleyCallback;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Services.Registration;
import com.MakemyBeauty.makemybeauty.Utilities.Dao;
import com.MakemyBeauty.makemybeauty.Utilities.DbHelper;
import com.android.volley.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.TimeZone;

public class RegisterActivity extends AppCompatActivity {

    TextView txtMobileNumber;
    EditText etDOB;
    Button btnCreateAccount;


    private int mDay;
    private int mMonth;
    private int mYear;

    private Boolean isetDateofBirth = false;

    UC uc_inst = UC.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        txtMobileNumber = (TextView)findViewById(R.id.txtMobileNumber);
        txtMobileNumber.setText(""+uc_inst.getInstance().getUser().getMobile());


        etDOB = (EditText)findViewById(R.id.etDOB);
        btnCreateAccount = (Button)findViewById(R.id.btnCreateAccount);



        etDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

                int selectedDay = calendar.get(Calendar.DAY_OF_MONTH);
                int selectedMonth = calendar.get(Calendar.MONTH);
                int selectedYear = calendar.get(Calendar.YEAR);


                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker view, int selectedYear,
                                                  int selectedMonth, int selectedDay) {
                                mDay = selectedDay;
                                mMonth = selectedMonth;
                                mYear = selectedYear;
                                StringBuilder Date = new StringBuilder("");
                                String conver = Integer.toString(selectedYear);
                                Date.append(conver);
                                Date.append("-");
                                selectedMonth++;
                                conver = Integer.toString(selectedMonth);
                                Date.append(conver);
                                Date.append("-");
                                conver = Integer.toString(selectedDay);
                                Date.append(conver);

                                etDOB.setText(Date.toString());
                                isetDateofBirth = true;
                            }
                        }, mDay, mMonth, mYear);

                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                final Calendar calendar2 = Calendar.getInstance();
                calendar2.set(1940,1,1);
                datePickerDialog.getDatePicker().setMinDate(calendar2.getTimeInMillis());
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();
            }
        });

        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etDOB.getText().toString().length() == 0) {
                    etDOB.setError("Enter dob");
                    return;
                }
                uc_inst.getUser().setDob(etDOB.getText().toString());

                try {

                    AsyncTask<String, Void, Integer> result = new Registration().execute();
                    int code = result.get();
                    System.out.println("jyo reg code" + code);
                    if (code == 200) {

                        DbHelper.createVendorDbHelper(RegisterActivity.this);
                        Dao vendorDetailsDao = new Dao();
                        vendorDetailsDao.getUserDetails();

                        Toast.makeText(RegisterActivity.this, "Successfully Registered", Toast.LENGTH_LONG).show();

                        Intent i = new Intent(RegisterActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        Toast.makeText(RegisterActivity.this, "Failed to Register", Toast.LENGTH_LONG).show();

                        // finish();
                    }
                } catch (Exception e) {

                    Toast.makeText(RegisterActivity.this, "Fail to Register", Toast.LENGTH_SHORT).show();
                }
            }

        });
    }

}
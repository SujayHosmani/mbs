package com.MakemyBeauty.makemybeauty.Activities.ui.menu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.MakemyBeauty.makemybeauty.Activities.SigninActivity;
import com.MakemyBeauty.makemybeauty.Activities.UserRegistrationActivity;
import com.MakemyBeauty.makemybeauty.Activities.UserotpVerificationActivity;
import com.MakemyBeauty.makemybeauty.R;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    TextView mLogoutBtn,mEditProfile, mName, mPh, mEmail, mCity;
    SharedPreferences prefs;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MenuFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MenuFragment newInstance(String param1, String param2) {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        mLogoutBtn = view.findViewById(R.id.logout);
        mName = view.findViewById(R.id.name);
        mPh = view.findViewById(R.id.pnno);
        mEmail = view.findViewById(R.id.mail);
        mCity = view.findViewById(R.id.city);
        mEditProfile = view.findViewById(R.id.editprofile);
        prefs = getContext().getSharedPreferences("profile", MODE_PRIVATE);
        String city,name,email,ph;
        name = prefs.getString("name","");
        city = prefs.getString("city","");
        ph = prefs.getString("mobile","");
        email = prefs.getString("email","");

        mName.setText(name);
        mCity.setText(city);
        mPh.setText(ph);
        mEmail.setText(email);

        mEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), UserRegistrationActivity.class);
                startActivity(intent);
            }
        });

        mLogoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("userid", "");
                editor.putString("name", "");
                editor.putString("dob", "");
                editor.putString("email", "");
                editor.putString("gender", "");
                editor.putString("token", "");
                editor.putBoolean("isLogged", false);
                editor.putString("mobile", "");
                editor.putString("address", "");
                editor.putString("city", "");
                editor.putString("latitude", "");
                editor.putString("longitude", "");
                editor.apply();
                Intent intent = new Intent(getContext(), SigninActivity.class);
                getActivity().finish();
                startActivity(intent);
            }
        });
        return view;
    }
}
package com.MakemyBeauty.makemybeauty.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.MakemyBeauty.makemybeauty.Adapter.LiveAdapter;
import com.MakemyBeauty.makemybeauty.Adapter.SubLiveAdapter;
import com.MakemyBeauty.makemybeauty.Model.GlobalApiModel;
import com.MakemyBeauty.makemybeauty.Model.SubCatModel;
import com.MakemyBeauty.makemybeauty.Model.SubServiceModel;
import com.MakemyBeauty.makemybeauty.Model.ValidateOtpModel;
import com.MakemyBeauty.makemybeauty.Network.NetworkClass;
import com.MakemyBeauty.makemybeauty.Network.VolleyCallback;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Utilities.AppConstant;
import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class SubServicesActivity extends AppCompatActivity {

    RecyclerView mService;
    String TAG = "SubServicesActivity";
    SubLiveAdapter msubLiveAdapter;
    SharedPreferences prefs;
    ArrayList<SubServiceModel> serviceCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_services);

        serviceCategories = new ArrayList<SubServiceModel>();
        mService = findViewById(R.id.re_service);

        LinearLayoutManager linearLayoutManagere = new LinearLayoutManager(this);
        linearLayoutManagere.setOrientation(LinearLayoutManager.VERTICAL);
        mService.setLayoutManager(linearLayoutManagere);
        prefs = getSharedPreferences("profile", MODE_PRIVATE);
        String mobile = prefs.getString("mobile", "");
        String token = prefs.getString("token", "");

        Intent intent = getIntent();
        String SubCat = intent.getStringExtra("subcat");
        String Cat = intent.getStringExtra("cat");

        if (SubCat != null && Cat != null){
            if (Cat.equals("1")){
                getServiceList(SubCat,mobile,token,"fetchcategoryMaleService");
            }else{
                getServiceList(SubCat,mobile,token,"fetchcategoryFemaleService");
            }

        }
    }

    private void getServiceList(String subCat, String mobile, String token, String subURL) {
        HashMap<String, String> maleSer = new HashMap<String, String>();
        maleSer.put("mobile", mobile);
        maleSer.put("token", token);
        maleSer.put("subcategory", subCat);
        NetworkClass.MakeRequest(AppConstant.url_base + subURL, maleSer, Request.Method.POST,   this, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) throws JSONException {
                Log.d(TAG,"the otp response: " + result);

                Gson gson= new Gson();
                ValidateOtpModel errModel = gson.fromJson(result.toString(), ValidateOtpModel.class);
                if (errModel.getErrorCode() == 200){
                    Type collectionType = new TypeToken<GlobalApiModel<SubServiceModel>>(){}.getType();
                    GlobalApiModel<SubServiceModel> obj = gson.fromJson(result.toString(), collectionType);
                    serviceCategories = obj.getData();
                    msubLiveAdapter = new SubLiveAdapter(SubServicesActivity.this,serviceCategories);
                    mService.setAdapter(msubLiveAdapter);
                }else{
                    Toast.makeText(SubServicesActivity.this, "" + errModel.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onError(String result) throws Exception {
                Log.d(TAG,"the otp Error: " + result);
                Toast.makeText(SubServicesActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
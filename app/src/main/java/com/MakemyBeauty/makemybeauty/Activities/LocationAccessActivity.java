package com.MakemyBeauty.makemybeauty.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.MakemyBeauty.makemybeauty.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationAccessActivity extends AppCompatActivity {

    Button btnTurnOnGps,btnGetLocation,btnNext;
    TextView text1,text2,text3,text4,text5;
    String mobile;
    FusedLocationProviderClient fusedLocationProvideClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_access);

        btnTurnOnGps = (Button)findViewById(R.id.btnTurnOnGps);
        btnGetLocation = (Button)findViewById(R.id.btnCurrentLocation);


        text1 = (TextView)findViewById(R.id.text1);
        text2 = (TextView)findViewById(R.id.text2);
        text3 = (TextView)findViewById(R.id.text3);
        text4 = (TextView)findViewById(R.id.text4);
        text5 = (TextView)findViewById(R.id.text5);



        // TODO: 16/9/20 : here we need change after api hit

        btnNext = (Button)findViewById(R.id.btnNext) ;
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(LocationAccessActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });


        fusedLocationProvideClient = LocationServices.getFusedLocationProviderClient(this);



        btnTurnOnGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSettingsAlert();
            }
        });


        btnGetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ActivityCompat.checkSelfPermission(LocationAccessActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    getLocation();

                } else {

                    ActivityCompat.requestPermissions(LocationAccessActivity.this
                            , new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);

                }
            }
        });

    }


    private void getLocation()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationProvideClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {

                Location location = task.getResult();

                if(location !=null){


                    try {
                        Geocoder geocoder = new Geocoder(LocationAccessActivity.this, Locale.getDefault());

                        List<Address> addresses = geocoder.getFromLocation(
                                location.getLatitude(),location.getLongitude(),1
                        );


                        text1.setText(Html.fromHtml(
                                "<font color = '#162252'><b><Latitude :</b><br></font>"
                                        +addresses.get(0).getLatitude()
                        ));

                        text2.setText(Html.fromHtml(
                                "<font color = '#162252'><b><Longitude :</b><br></font>"
                                        +addresses.get(0).getLongitude()
                        ));

                        text3.setText(Html.fromHtml(
                                "<font color = '#162252'><b><Country Name :</b><br></font>"
                                        +addresses.get(0).getCountryName()
                        ));
                        text4.setText(Html.fromHtml(
                                "<font color = '#162252'><b><Locality :</b><br></font>"
                                        +addresses.get(0).getLocality()
                        ));


                        text5.setText(Html.fromHtml(
                                "<font color = '#ba2255'><b><Address :</b><br></font>"
                                        +addresses.get(0).getAddressLine(0)
                        ));



                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
    }

    private void showSettingsAlert()
    {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                LocationAccessActivity.this);
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        LocationAccessActivity.this.startActivity(intent);


                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }


}
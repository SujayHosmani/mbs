package com.MakemyBeauty.makemybeauty.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.MakemyBeauty.makemybeauty.Adapter.HomeAdapter;
import com.MakemyBeauty.makemybeauty.Adapter.ServiceAdapter;
import com.MakemyBeauty.makemybeauty.Adapter.ViewPagerAdapter;
import com.MakemyBeauty.makemybeauty.Model.UC;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Services.Femaleservice;
import com.MakemyBeauty.makemybeauty.Services.Maleservice;
import com.MakemyBeauty.makemybeauty.Utilities.GridDecorator;

import com.MakemyBeauty.makemybeauty.fragments.FooterFragment;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class HomeActivity extends AppCompatActivity {

    //Todo: Mbs user sucessfully runnig
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    TextView etLocation;
    int id, id1= 0;

    TextView textMale;

    RecyclerView categories,categories2;
    HomeAdapter categoryAdapter;



    Button btnMale, btnFemale;





//    TabLayout tabLayout;
//    FrameLayout frameLayout;
//  //  Fragment fragment = null;
//    FragmentManager fragmentManager;
//    FragmentTransaction fragmentTransaction;

//    GridView simplegridView;
//    ArrayList Category=new ArrayList<>();
//        GridView simplegridView;
//
//        String[] values = {"one","two","three","four","five","six"};
//        int[] imgaes = {R.drawable.number_one,R.drawable.number_two,R.drawable.number_three,R.drawable.number_four,R.drawable.number_one,R.drawable.number_two,};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        UC dg_inst = UC.getInstance();


        Fragment fragment = new FooterFragment();
        FragmentManager fm1 = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm1.beginTransaction();
        fragmentTransaction.replace(R.id.framelayout, fragment);
        fragmentTransaction.commit();

        viewPager = (ViewPager) findViewById(R.id.im_slider1);
        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots);
        etLocation = findViewById(R.id.etLocation);
        netWorkAlert(this);

        //  textMale = findViewById(R.id.textMale);


        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this);


        viewPager.setAdapter(viewPagerAdapter);


        dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[dotscount];
        final int[] currentPage = {0};


        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dots));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);

        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dots));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dots));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dots));

            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage[0] == dots.length) {
                    currentPage[0] = 0;
                }
                viewPager.setCurrentItem(currentPage[0]++, true);
            }
        };


        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);

//        Intent mIntent = getIntent();
//        final int catId = mIntent.getIntExtra("catId", 0);
//        final int pos = mIntent.getIntExtra("Position", 0);

        final Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);
        id1 = intent.getIntExtra("subcat_id", 0);

        categories = (RecyclerView) findViewById(R.id.sub_category);
        categories.setLayoutManager(new GridLayoutManager(getApplication(), 4));
        int spanCount = 2; // 3 columns
        int spacing = 2; // 50px
        boolean includeEdge = false;


        AsyncTask<Context, Void, Integer> result2 = new Maleservice().execute(HomeActivity.this);
        try {

            int code = result2.get();
            System.out.println("jyo code" + code);

            Toast.makeText(HomeActivity.this, "Application Fetching", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

            Toast.makeText(HomeActivity.this, "Application Fail to fetch", Toast.LENGTH_SHORT).show();
        }
        categories.addItemDecoration(new GridDecorator(spanCount, spacing, includeEdge));
        categoryAdapter = new HomeAdapter(HomeActivity.this, UC.getInstance().getCategoryList(),new HomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                UC.getInstance().setCatId(UC.getInstance().getCategoryList().get(position).getCatId());
                UC.getInstance().setSubcatId(UC.getInstance().getCategoryList().get(position).getSubcatId());

                Intent intent = new Intent(HomeActivity.this, ServicesActivity.class);
                intent.putExtra("id", UC.getInstance().getCategoryList().get(position).getCatId());
                intent.putExtra("subcat_id", UC.getInstance().getCategoryList().get(position).getSubcatId());
                intent.putExtra("Position", position);
                startActivity(intent);

//                if (UC.getInstance().getCategoryList().get(position).getCatId().equals("1") ) {
                   // Intent intent = new Intent(HomeActivity.this, ServicesActivity.class);
//                    intent.putExtra("catId", UC.getInstance().getCategoryList().get(position).getCatId());
//                    intent.putExtra("Position", position);
//
                    //System.out.println("jyo cat name"+BB.getInstance().getCategoryList().get(position).getCatId());
                  //  startActivity(intent);
//                }
            }
        });
        categories.setAdapter(categoryAdapter);



//        Intent mIntent2 = getIntent();
//        final int catId2 = mIntent2.getIntExtra("catId", 0);
//        final int pos2 = mIntent2.getIntExtra("Position", 0);

        categories2 = (RecyclerView) findViewById(R.id.sub_category2);
        categories2.setLayoutManager(new GridLayoutManager(getApplication(), 4));
        int spanCount2 = 2; // 3 columns
        int spacing2= 2; // 50px
        boolean includeEdge2 = false;




        AsyncTask<Context, Void, Integer> result = new Femaleservice().execute(HomeActivity.this);
        try {

            int code = result.get();
            System.out.println("jyo code" + code);
        } catch (Exception e) {

            Toast.makeText(HomeActivity.this, "Application Fail to fetch", Toast.LENGTH_SHORT).show();
        }
        categories2.addItemDecoration(new GridDecorator(spanCount2, spacing2, includeEdge2));
        categoryAdapter = new HomeAdapter(HomeActivity.this, UC.getInstance().getCategoryList2(),new HomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

               UC.getInstance().setCatId(UC.getInstance().getCategoryList2().get(position).getCatId());
                UC.getInstance().setSubcatId(UC.getInstance().getCategoryList2().get(position).getSubcatId());

                Intent intent = new Intent(HomeActivity.this, ServicesActivity.class);
                intent.putExtra("id", UC.getInstance().getCategoryList2().get(position).getCatId());
                intent.putExtra("subcat_id", UC.getInstance().getCategoryList2().get(position).getSubcatId());
                intent.putExtra("Position", position);
                startActivity(intent);
//                if (UC.getInstance().getCategoryList().get(position).getCatId().equals("2") )
////                {
//                    Intent intent = new Intent(HomeActivity.this, ServicesActivity.class);
//                    intent.putExtra("catId", UC.getInstance().getCategoryList().get(position).getCatId());
//                    intent.putExtra("Position", position);
                    //System.out.println("jyo cat name"+BB.getInstance().getCategoryList().get(position).getCatId());
//                    startActivity(intent);
//                }
//                else{
//                    Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
//                    startActivity(intent);
//                }
            }
        });
        categories2.setAdapter(categoryAdapter);
    }

    public  static void netWorkAlert(final HomeActivity context){

        if (isOnline(context)) {
            //do whatever you want to do
        } else {
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle("Info");
                alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        context.finish();
                    }
                });

                alertDialog.show();
            } catch (Exception e) {
                Log.e("Constants.TAG", "Show Dialog: " + e.getMessage());
            }
        }
    }

    private static boolean isOnline(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager)context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            // Toast.makeText(context, "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        UC ht_inst = UC.getInstance();
        if(!ht_inst.getInitDone()) {
            Intent intent = new Intent(HomeActivity.this, Spalsh_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        super.onResume();
    }

}





             /*   categoryAdapter = new HomeAdapter(HomeActivity.this, UC.getInstance().getCategoryList(), new HomeAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                      *//*  Intent intent = new Intent(HomeActivity.this,ProductActivity.class);
                        startActivity(intent);*//*

                        //   catSel = AG.getInstance().getCategoryList().get(position).getCatId();

                        //   subcategoryAdapter.setData(AG.getInstance().getSubcatMapping().get(AG.getInstance().getCategoryList().get(position).getCatId()));

                    }
                });
                categories.setLayoutManager(llm1);
                categories.setAdapter(categoryAdapter);

            }
        });*/


            //btnFemale = (Button)findViewById(R.id.btnFemale);

      /*  btnFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              //  loadFragment(new Female());
                // loadFragment(new Male());

             *//*   Intent mIntent = getIntent();
                final int catId = mIntent.getIntExtra("category", 0);
                final int subcategory_id = mIntent.getIntExtra("subcategory_id", 0);
                final int subcategory = mIntent.getIntExtra("subcategory", 0);



                AsyncTask<Context, Void, Integer> result2 = new Femaleservice().execute(HomeActivity.this);
                try {

                    int code = result2.get();
                    System.out.println("jyo code" + code);
                    Toast.makeText(HomeActivity.this, "Application Fetching ", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {

                    Toast.makeText(HomeActivity.this, "Application Fail to fetch", Toast.LENGTH_SHORT).show();
                }

                categories.addItemDecoration(new GridDecorator(spanCount, spacing, includeEdge));
                categories.setLayoutManager(new GridLayoutManager(getApplication(), 1));
                categories.setLayoutManager(new LinearLayoutManager(getApplication(), LinearLayoutManager.VERTICAL, false));
                categories.setAdapter(categoryAdapter);*//*


            }
        });


    }*/
/*

   private void loadFragment(Fragment fragment) {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frameLayout, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
*/


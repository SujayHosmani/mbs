package com.MakemyBeauty.makemybeauty.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.MakemyBeauty.makemybeauty.Adapter.OrderHistoryAdapter;
import com.MakemyBeauty.makemybeauty.Model.UC;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Services.FetchOrderHistoryview;

public class MyOrderHistoryActivity extends AppCompatActivity {

    OrderHistoryAdapter list_adapter;
    ListView listView;
    TextView mModuleTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_product, null, false);
        //mDrawer.addView(view, 0);

//        mModuleTitle = (TextView) findViewById(R.id.product_title);
//        mModuleTitle.setText("My Orders");

        AsyncTask<Context, Void, Integer> result1 = new FetchOrderHistoryview().execute(MyOrderHistoryActivity.this);
        try {

            int code = result1.get();
            System.out.println("jyo code" + code);

        } catch (Exception e) {

            Toast.makeText(MyOrderHistoryActivity.this, "Application Fail to fetch", Toast.LENGTH_SHORT).show();
        }

        //listView = view.findViewById(R.id.listView);
        list_adapter = new OrderHistoryAdapter(MyOrderHistoryActivity.this, UC.getInstance().getOrderHistorylist());
        listView.setAdapter(list_adapter);

        listView.setAdapter(list_adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Intent intent=new Intent(MyOrderActivity.this,OrderListReviewActivity.class);
//                intent.putExtra("id", BB.getInstance().getOrderHistorylist().get(position).getOrder_id());
//                System.out.println("jyo order id"+ BB.getInstance().getOrderHistorylist().get(position).getOrder_id());
//                startActivity(intent);
            }
        });
        list_adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        UC ht_inst = UC.getInstance();
        if(!ht_inst.getInitDone()) {
            Intent intent = new Intent(MyOrderHistoryActivity.this, Spalsh_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        super.onResume();
    }
}

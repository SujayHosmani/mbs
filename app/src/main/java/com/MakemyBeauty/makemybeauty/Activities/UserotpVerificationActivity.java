package com.MakemyBeauty.makemybeauty.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.MakemyBeauty.makemybeauty.Model.UC;
import com.MakemyBeauty.makemybeauty.Model.ValidateOtpModel;
import com.MakemyBeauty.makemybeauty.Network.NetworkClass;
import com.MakemyBeauty.makemybeauty.Network.VolleyCallback;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Services.UserMobileVerified;
import com.MakemyBeauty.makemybeauty.Services.UserOtpValidation;
import com.MakemyBeauty.makemybeauty.Utilities.AppConstant;
import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;

public class UserotpVerificationActivity extends AppCompatActivity {

    EditText mmobile, motp;
    Button btn_signup;
    String mobile;
    TextView mSignIn;
    int flag = 1;
    private String otp_gen;
    TextView mResendBtn;
    String TAG = "UserotpVerificationActivity";
    SharedPreferences prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userotp_verification);
        intializeView();
        mResendBtn.setVisibility(View.GONE);
        prefs = getSharedPreferences("profile", MODE_PRIVATE);

        mSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserotpVerificationActivity.this, SigninActivity.class);
                finish();
                startActivity(intent);
            }
        });

    }


    private void sendOTP(){
        mobile = mmobile.getText().toString();
        if (motp.getText().toString().length() == 0) {
            motp.setError("Enter otp");
        }
        UC.getInstance().getUser().setOtp(motp.getText().toString());

        if (flag == 1) {
            if (mobile.isEmpty()) {
                Toast.makeText(UserotpVerificationActivity.this, "Please enter the phone number", Toast.LENGTH_SHORT).show();
            } else if (mobile.length() < 10) {
                Toast.makeText(UserotpVerificationActivity.this, "Please enter 10 digits number", Toast.LENGTH_SHORT).show();
            } else {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("mobile", mobile);
                NetworkClass.MakeRequest(AppConstant.url_base + "validateMobileReg", params, Request.Method.POST,   UserotpVerificationActivity.this, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject result) throws JSONException {
                        Gson gson= new Gson();
                        ValidateOtpModel obj = gson.fromJson(result.toString(), ValidateOtpModel.class);
                        Toast.makeText(UserotpVerificationActivity.this, "" + obj.getMessage(), Toast.LENGTH_SHORT).show();
                        if (obj.getErrorCode() == 203){
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("userid", obj.getUser_id());
                            editor.putString("mobile", mobile);
                            editor.putString("token", obj.getToken());
                            editor.apply();

                            Intent intent = new Intent(UserotpVerificationActivity.this, UserRegistrationActivity.class);
                            startActivity(intent);
                        }else{
                            mResendBtn.setVisibility(View.VISIBLE);
                            motp.setVisibility(View.VISIBLE);
                            btn_signup.setText("verify otp");
                            flag = 2;
                        }

                    }

                    @Override
                    public void onError(String result) throws Exception {
                        Toast.makeText(UserotpVerificationActivity.this, "Errorzz: " + result, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {

            if (motp.getText().length() != 4) {
                Toast.makeText(UserotpVerificationActivity.this, "Enter Valid OTP", Toast.LENGTH_LONG).show();
                return;
            }


            //
            HashMap<String, String> otpVerify = new HashMap<String, String>();
            otpVerify.put("mobile", mobile);
            otpVerify.put("otp", motp.getText().toString());
            NetworkClass.MakeRequest(AppConstant.url_base + "verifyOtpRegistration", otpVerify, Request.Method.POST,   UserotpVerificationActivity.this, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject result) throws JSONException {
                    Log.d(TAG,"the otp response: " + result);
                    Gson gson= new Gson();
                    ValidateOtpModel obj = gson.fromJson(result.toString(), ValidateOtpModel.class);
                    if (obj.getErrorCode() == 200){
                        // store user id to pref
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("userid", obj.getUser_id());
                        editor.putString("mobile", mobile);
                        editor.putString("token", obj.getToken());
                        editor.apply();

                        Intent intent = new Intent(UserotpVerificationActivity.this, UserRegistrationActivity.class);
                        //finish();
                        startActivity(intent);
                    }
                    Toast.makeText(UserotpVerificationActivity.this, "" + obj.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(String result) throws Exception {
                    Log.d(TAG,"the otp Error: " + result);
                    Toast.makeText(UserotpVerificationActivity.this, "Errorzz: " + result, Toast.LENGTH_SHORT).show();
                }
            });

        }
    }

    private void intializeView() {
        mSignIn = findViewById(R.id.signin_btn);
        mResendBtn = findViewById(R.id.resend);
        mmobile = findViewById(R.id.etMobileNumber);
        motp = findViewById(R.id.etOtp);
        motp.setVisibility(View.GONE);
        btn_signup = findViewById(R.id.btnVerifyOtp);

        mResendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                sendOTP();
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendOTP();
            }
        });

    }
}



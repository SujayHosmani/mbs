package com.MakemyBeauty.makemybeauty.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.MakemyBeauty.makemybeauty.Model.User;
import com.MakemyBeauty.makemybeauty.Model.ValidateOtpModel;
import com.MakemyBeauty.makemybeauty.Network.NetworkClass;
import com.MakemyBeauty.makemybeauty.Network.VolleyCallback;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Utilities.AppConstant;
import com.android.volley.Request;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class UserRegistrationActivity extends AppCompatActivity {

    EditText mName,mDob,mEmail,mPass;
    RadioGroup mGenderGroup;
    RadioButton mMale,mFemale;
    CheckBox mTerms;
    Button mSubmit;

    String Gen = "";
    String TAG = "UserotpVerificationActivity";
    String city = "", address = "";
    Boolean isLocationFetched = false, isetDateofBirth = false;
    Double longitude = 0.0,latitude = 0.0;
    SharedPreferences prefs;
    FusedLocationProviderClient fusedLocationProvideClient;
    private final int REQUEST_LOCATION_PERMISSION = 1;
    private int mDay;
    private int mMonth;
    private int mYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
        initializeViews();

        prefs = getSharedPreferences("profile", MODE_PRIVATE);
        fusedLocationProvideClient = LocationServices.getFusedLocationProviderClient(this);
        getCurrentLocation();

        mDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

                int selectedDay = calendar.get(Calendar.DAY_OF_MONTH);
                int selectedMonth = calendar.get(Calendar.MONTH);
                int selectedYear = calendar.get(Calendar.YEAR);


                DatePickerDialog datePickerDialog = new DatePickerDialog(UserRegistrationActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker view, int selectedYear,
                                                  int selectedMonth, int selectedDay) {
                                mDay = selectedDay;
                                mMonth = selectedMonth;
                                mYear = selectedYear;
                                StringBuilder Date = new StringBuilder("");
                                String conver = Integer.toString(selectedYear);
                                Date.append(conver);
                                Date.append("-");
                                selectedMonth++;
                                conver = Integer.toString(selectedMonth);
                                Date.append(conver);
                                Date.append("-");
                                conver = Integer.toString(selectedDay);
                                Date.append(conver);

                                mDob.setText(Date.toString());
                                isetDateofBirth = true;
                            }
                        }, mDay, mMonth, mYear);

                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                final Calendar calendar2 = Calendar.getInstance();
                calendar2.set(1940,1,1);
                datePickerDialog.getDatePicker().setMinDate(calendar2.getTimeInMillis());
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();
            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String name,dob,mail,pass;
                name = mName.getText().toString();
                dob = mDob.getText().toString();
                mail = mEmail.getText().toString();
                pass = mPass.getText().toString();

                if (name.isEmpty() || dob.isEmpty() || mail.isEmpty() || pass.isEmpty() || !isGenderSelected() || !isTermsAccepted()){
                    if (name.isEmpty()){
                        mName.setError("Enter name");
                    }
                    if (dob.isEmpty()){
                        mDob.setError("Enter dob");
                    }
                    if (mail.isEmpty()){
                        mEmail.setError("Enter mail");
                    }
                    if (pass.isEmpty()){
                        mPass.setError("Enter pass");
                    }
                    if (!isGenderSelected()){
                        Toast.makeText(UserRegistrationActivity.this, "Please select gender", Toast.LENGTH_SHORT).show();
                    }
                    if (!isTermsAccepted()){
                        Toast.makeText(UserRegistrationActivity.this, "Please accept the terms", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if (mMale.isChecked()){
                        Gen = "male";
                    }else if(mFemale.isChecked()){
                        Gen = "female";
                    }
                    String mobile = prefs.getString("mobile","");
                    String token = prefs.getString("token", "");
                    String userId = prefs.getString("userid", "");
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("user_id", userId);
                    params.put("name", name);
                    params.put("email", mail);
                    params.put("pwd", pass);
                    params.put("gender", Gen);
                    params.put("dob", dob);
                    params.put("mobile", mobile);
                    params.put("token", token);
                    if (isLocationFetched){
                        params.put("latitude", String.valueOf(latitude));
                        params.put("longitude", String.valueOf(longitude));
                        params.put("address", address);
                        params.put("city", city);
                    }


                    NetworkClass.MakeRequest(AppConstant.url_base + "profileDetailsUpdate", params, Request.Method.POST,   UserRegistrationActivity.this, new VolleyCallback() {
                        @Override
                        public void onSuccess(JSONObject result) throws JSONException {
                            Gson gson= new Gson();
                            ValidateOtpModel obj = gson.fromJson(result.toString(), ValidateOtpModel.class);
                            Toast.makeText(UserRegistrationActivity.this, ""+obj.getMessage(), Toast.LENGTH_SHORT).show();
                            if (obj.getErrorCode() == 200){
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("name", name);
                                editor.putString("dob", dob);
                                editor.putString("mail", mail);
                                editor.putString("gender", Gen);
                                editor.apply();
                                Intent intent = new Intent(UserRegistrationActivity.this,SigninActivity.class);
                                finish();
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onError(String result) throws Exception {
                            Toast.makeText(UserRegistrationActivity.this, "Errorzz: " + result, Toast.LENGTH_SHORT).show();
                        }
                    });


                }
            }
        });

    }





    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(UserRegistrationActivity.this, "no permission", Toast.LENGTH_SHORT).show();
            requestLocationPermission();
            return;
        }
        fusedLocationProvideClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {

                Location location = task.getResult();

                if(location !=null){
                    //Toast.makeText(UserRegistrationActivity.this, "location is null", Toast.LENGTH_SHORT).show();

                    try {
                        Geocoder geocoder = new Geocoder(UserRegistrationActivity.this, Locale.getDefault());

                        List<Address> addresses = geocoder.getFromLocation(
                                location.getLatitude(),location.getLongitude(),1
                        );
                        if (addresses.size() > 0){
                            isLocationFetched = true;
                            latitude = addresses.get(0).getLatitude();
                            longitude = addresses.get(0).getLongitude();
                            address = addresses.get(0).getAddressLine(0);
                            city = addresses.get(0).getAdminArea();
                            //Toast.makeText(UserRegistrationActivity.this, ""+address, Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
    }

    private void requestLocationPermission() {
        if (ActivityCompat.checkSelfPermission(UserRegistrationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(UserRegistrationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(UserRegistrationActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    // User Allows Permission access
                    getCurrentLocation();

                } else {

                    // User denies Permission access

                }
        }
    }



    private boolean isTermsAccepted() {
        return mTerms.isChecked();
    }

    private boolean isGenderSelected() {
        return mMale.isChecked() || mFemale.isChecked();
    }

    private void initializeViews() {
        mName = findViewById(R.id.full_name);
        mDob = findViewById(R.id.dob);
        mTerms = findViewById(R.id.terms_check);
        mEmail = findViewById(R.id.email);
        mPass = findViewById(R.id.password);
        mGenderGroup = findViewById(R.id.genderGroup);
        mMale = findViewById(R.id.male_check);
        mFemale = findViewById(R.id.female_check);
        mSubmit = findViewById(R.id.submit_btn);
    }
}
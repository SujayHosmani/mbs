package com.MakemyBeauty.makemybeauty.Activities.ui.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.MakemyBeauty.makemybeauty.Activities.UserRegistrationActivity;
import com.MakemyBeauty.makemybeauty.Activities.UserotpVerificationActivity;
import com.MakemyBeauty.makemybeauty.Adapter.LiveAdapter;
import com.MakemyBeauty.makemybeauty.Adapter.ViewPagerAdapter;
import com.MakemyBeauty.makemybeauty.Interface.Ichild;
import com.MakemyBeauty.makemybeauty.Model.Category;
import com.MakemyBeauty.makemybeauty.Model.GlobalApiModel;
import com.MakemyBeauty.makemybeauty.Model.ProfileModel;
import com.MakemyBeauty.makemybeauty.Model.SubCatModel;
import com.MakemyBeauty.makemybeauty.Model.ValidateOtpModel;
import com.MakemyBeauty.makemybeauty.Network.NetworkClass;
import com.MakemyBeauty.makemybeauty.Network.VolleyCallback;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Utilities.AppConstant;
import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;


public class HomeFragment extends Fragment {
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    RecyclerView mMaleView,mFemaleView;
    String TAG = "HomeFragment";
    ScrollView mScrollView;
    LiveAdapter liveAdapter,femaleAdapter;
    SharedPreferences prefs;
    ArrayList<SubCatModel> maleCategories,femaleCategories;
    private HomeViewModel homeViewModel;
    String images2 [] = {"https://images.unsplash.com/photo-1503676260728-1c00da094a0b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        viewPager = root.findViewById(R.id.vpager);
        prefs = getContext().getSharedPreferences("profile", MODE_PRIVATE);
        maleCategories = new ArrayList<SubCatModel>();
        femaleCategories = new ArrayList<SubCatModel>();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPagerAdapter = new ViewPagerAdapter(view.getContext());
        viewPager.setAdapter(viewPagerAdapter);
        mMaleView = view.findViewById(R.id.re_male);
        mFemaleView = view.findViewById(R.id.re_female);
        LinearLayoutManager linearLayoutManagere = new LinearLayoutManager(getActivity());
        linearLayoutManagere.setOrientation(LinearLayoutManager.HORIZONTAL);
        LinearLayoutManager linearLayoutManagere2 = new LinearLayoutManager(getActivity());
        linearLayoutManagere2.setOrientation(LinearLayoutManager.HORIZONTAL);
        mMaleView.setLayoutManager(linearLayoutManagere);
        mFemaleView.setLayoutManager(linearLayoutManagere2);

        mMaleView.setNestedScrollingEnabled(false);
        mFemaleView.setNestedScrollingEnabled(false);

//        mFemaleView.hasFixedSize();
//        mMaleView.hasFixedSize();

        fetchMaleServices();
        fetchFeMaleServices();
    }

    private void fetchMaleServices(){
        final String mobile = prefs.getString("mobile", "");
        final String token = prefs.getString("token", "");
        HashMap<String, String> maleSer = new HashMap<String, String>();
        maleSer.put("mobile", mobile);
        maleSer.put("token", token);

        NetworkClass.MakeRequest(AppConstant.url_base + "fetchmaleSubCategory", maleSer, Request.Method.POST,   getContext(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) throws JSONException {
                Log.d(TAG,"the otp response: " + result);

                Gson gson= new Gson();
                ValidateOtpModel errModel = gson.fromJson(result.toString(), ValidateOtpModel.class);
                if (errModel.getErrorCode() == 200){
                    Type collectionType = new TypeToken<GlobalApiModel<SubCatModel>>(){}.getType();
                    GlobalApiModel<SubCatModel> obj = gson.fromJson(result.toString(), collectionType);
                    maleCategories = obj.getData();
                    liveAdapter = new LiveAdapter(getContext(),maleCategories);
                    mMaleView.setAdapter(liveAdapter);
                }else{
                    Toast.makeText(getContext(), "" + errModel.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onError(String result) throws Exception {
                Log.d(TAG,"the otp Error: " + result);
                Toast.makeText(getContext(), "Error: " + result, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchFeMaleServices(){
        HashMap<String, String> otpVerify = new HashMap<String, String>();
        final String mobile = prefs.getString("mobile", "");
        final String token = prefs.getString("token", "");
        otpVerify.put("mobile", mobile);
        otpVerify.put("token", token);
        NetworkClass.MakeRequest(AppConstant.url_base + "fetchFemaleSubCategory", otpVerify, Request.Method.POST,   getContext(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) throws JSONException {
                Log.d(TAG,"the otp response: " + result);
                Gson gson= new Gson();
                ValidateOtpModel errModel = gson.fromJson(result.toString(), ValidateOtpModel.class);
                if (errModel.getErrorCode() == 200){
                    Type collectionType = new TypeToken<GlobalApiModel<SubCatModel>>(){}.getType();
                    GlobalApiModel<SubCatModel> obj = gson.fromJson(result.toString(), collectionType);
                    femaleCategories = obj.getData();
                    femaleAdapter = new LiveAdapter(getContext(),femaleCategories);
                    mFemaleView.setAdapter(femaleAdapter);
                }else{
                    Toast.makeText(getContext(), "" + errModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String result) throws Exception {
                Log.d(TAG,"the otp Error: " + result);
                Toast.makeText(getContext(), "Error: " + result, Toast.LENGTH_SHORT).show();
            }
        });
    }


}
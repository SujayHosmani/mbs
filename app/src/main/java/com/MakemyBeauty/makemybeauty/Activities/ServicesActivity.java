package com.MakemyBeauty.makemybeauty.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.MakemyBeauty.makemybeauty.Adapter.ServiceAdapter;
import com.MakemyBeauty.makemybeauty.Model.Products;
import com.MakemyBeauty.makemybeauty.Model.Service;
import com.MakemyBeauty.makemybeauty.Model.UC;
import com.MakemyBeauty.makemybeauty.R;
import com.MakemyBeauty.makemybeauty.Services.Addcart;
import com.MakemyBeauty.makemybeauty.Services.CartCount;
import com.MakemyBeauty.makemybeauty.Services.FetchServices;
import com.MakemyBeauty.makemybeauty.Services.Updatecart;

public class ServicesActivity extends AppCompatActivity {

    // Todo: Fetching Service (Based on Category and Subcategory)

    //ListView listView;
    RecyclerView view_product;
    int id, id1 = 0;


    int sid;
    String sname, squantity, price, s_image;

    // ScrollView scrollView;
    ServiceAdapter serviceAdapter;
    Button btnaddtocart;
    ProgressBar progressBar;
    TextView mModuleTitle, tvshop, mcart, cart_quantity;
    ImageView ivfilter;
    //    ProgressBar progressBar;
    LinearLayout llcartempty;
    ListView listView;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        //mDrawer.addView(view, 0);
        //  netWorkAlert(this);

        //  listView = (ListView)findViewById(R.id.listView);

        //  scrollView = (ScrollView)findViewById(R.id.scrollView) ;

//        services = (RecyclerView) findViewById(R.id.sub_category1);
//        services.setLayoutManager(new GridLayoutManager(getApplication(), 4));
//        int spanCount = 2; // 3 columns
//        int spacing = 2; // 50px
//        boolean includeEdge = false;

        //Todo: commemted

        //   view_product = findViewById(R.id.view_product);

//        UC dg_inst = UC.getInstance();
//
//         Intent intent = getIntent();
//        id = intent.getIntExtra("id", 0);
//        id1 = intent.getIntExtra("subid", 0);
//
//         dg_inst.getCategory().setCatIdd(id);
//         dg_inst.getCategory().setSub_catId(id1);
//
//        FetchServices fetchServices = new FetchServices();
//        fetchServices.id1 = id;
//        fetchServices.id2= id1;
        //Todo: commemted

        view_product = (RecyclerView) findViewById(R.id.view_product);
        view_product.setLayoutManager(new GridLayoutManager(getApplication(), 1));
        int spanCount = 1; // 3 columns
        int spacing = 1; // 50px
        boolean includeEdge = false;

        final UC dg_inst = UC.getInstance();


        Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);
        id1 = intent.getIntExtra("subcat_id", 0);

        FetchServices fetchServices = new FetchServices();

        fetchServices.execute(id, id1);
        try {

            Toast.makeText(ServicesActivity.this, "Fetchg service Data", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

            Toast.makeText(ServicesActivity.this, "Application Fail to fetch Service data", Toast.LENGTH_SHORT).show();

        }


        //Todo: Click on Service

        serviceAdapter = new ServiceAdapter(ServicesActivity.this, UC.getInstance().getProductList2(), new ServiceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        LinearLayoutManager llm = new LinearLayoutManager(this);


        view_product.setLayoutManager(llm);
        view_product.setAdapter(serviceAdapter);


        // Todo: Add to Cart (Service)
        Intent intent2 = getIntent();
        sid = intent2.getIntExtra("s_id", 0);
        sname = intent2.getStringExtra("s_name");
        squantity = intent2.getStringExtra("pro_qnty");
        price = intent2.getStringExtra("s_price");
        s_image = intent2.getStringExtra("s_image");


        //   subcategories= findViewById(R.id.sub_cat);d

        Addcart addCart = new Addcart();


        addCart.execute(String.valueOf(sid), sname, squantity, price, s_image);
        try {


            Toast.makeText(ServicesActivity.this, "Successfully Added to cart", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

            Toast.makeText(ServicesActivity.this, "Application Fail to fetch", Toast.LENGTH_SHORT).show();

        }


    }
}























        // Todo: Cart Start---------------------------

//
////        mModuleTitle = (TextView)findViewById(R.id.product_title);
////        mModuleTitle.setText("");
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//    //        ivfilter=toolbar.findViewById(R.id.iv_filter);
//    //      ivfilter.setVisibility(View.GONE);
//        //        mcart=toolbar.findViewById(R.id.header_cart);
//      //  mcart.setVisibility(View.VISIBLE);
//        cart_quantity=toolbar.findViewById(R.id.tv_cartValue);
//        cart_quantity.setVisibility(View.VISIBLE);
////        llcartempty=findViewById(R.id.llCartEmpty);
//        //listView = (ListView) findViewById(R.id.listView);
//        tvshop=findViewById(R.id.tv_continue_shopping);
//
//        progressBar = (ProgressBar) findViewById(R.id.progress);
//        netWorkAlert(this);
//
//
//        AsyncTask<Context, Void, Integer> result2 = new CartCount().execute(ServicesActivity.this);
//        try {
//            int code = result2.get();
//            System.out.println("jyo code" + code);
//        } catch (Exception e) {
//
//            Toast.makeText(ServicesActivity.this, "Application Fail to fetch", Toast.LENGTH_SHORT).show();
//        }
//
//        for(int i = 0; i < UC.getInstance().getCartItems().size(); i++) {
//            for(int j = 0; j < UC.getInstance().getmProductList().size(); j++) {
//                System.out.println("jyo product id "+ UC.getInstance().getmProductList().get(j).getCart_id() +" cart "+ UC.getInstance().getCartItems().get(i).getCart_id());
//                if(UC.getInstance().getCartItems().get(i).getPro_id() == UC.getInstance().getmProductList().get(j).getPro_id())
//                    UC.getInstance().getmProductList().get(j).setQuantity(UC.getInstance().getCartItems().get(i).getQuantity());
//            }
//        }
//
//
////        mcart.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                if(dg_inst.getCartItems().size()==0){
////                    llcartempty.setVisibility(View.VISIBLE);
////                    listView.setVisibility(View.GONE);
////
////                    //Toast.makeText(ProductsActivity.this,"cart is zero",Toast.LENGTH_SHORT).show();
////
////                }else {
////
////                    Intent intent = new Intent(getApplication(), CartActivity.class);
////                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                    startActivity(intent);
////                }
////            }
////        });
////        tvshop.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                llcartempty.setVisibility(View.GONE);
////                listView.setVisibility(View.VISIBLE);
////            }
////        });
//
//        int gCount = 0;
//        for(int i = 0; i < UC.getInstance().getCartItems().size(); i++) {
//            gCount += UC.getInstance().getCartItems().get(i).getQuantity();
//        }
//        cart_quantity.setText(""+gCount);
//        UC.getInstance().setCartqunatity(gCount);
//
//        Intent mIntent = getIntent();
//        final int subcatId = mIntent.getIntExtra("subcatId", 0);
//        final int serviceId = mIntent.getIntExtra("serviceId",0);
//        final int pos = mIntent.getIntExtra("Position1", 0);
//
//
//        serviceAdapter =new ServiceAdapter(ServicesActivity.this, UC.getInstance().getProductList2(), new ServiceAdapter.MyAdapterListener() {
//
//
//            @Override
//            public void onPlusButtonClick(View v, int position) {
////                progressBar.setVisibility(View.VISIBLE);
//                // System.out.println("jyo in activity onPlusButtonClick"+position);
//                Products item;
//                Integer localQuantity;
//                UC Ht_inst = UC.getInstance();
//                // Log.d("jyo ", "plus button clicked");
//                item = UC.getInstance().getmProductList().get(position);
//                localQuantity = item.getQuantity();
//                localQuantity++;
//                item.setQuantity(localQuantity);
//                if (!Ht_inst.getCartItems().contains(item)) {
//                    Ht_inst.getCartItems().add(item);
//                }
//
//                int g_cart = Ht_inst.getCartqunatity();
//                g_cart++;
//                Ht_inst.setCartqunatity(g_cart);
//                cart_quantity.setText("" + g_cart);
//                serviceAdapter.notifyDataSetChanged();
//
//                Ht_inst.setAddproducts(item);
//
//                AsyncTask<Context, Void, Integer> result = new Updatecart().execute(ServicesActivity.this);
//                try {
//
//                    int code = result.get();
//                    // System.out.println("jyo code" + code);
//                    if(code==200){
//                        Toast.makeText(ServicesActivity.this, "Added successfully", Toast.LENGTH_SHORT).show();
//                    }
//                } catch (Exception e) {
//                    Toast.makeText(ServicesActivity.this, "Application Fail to add", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//
//            @Override
//            public void onMinusButtonClick(View v, int position) {
//                //  progressBar.setVisibility(View.VISIBLE);
//
//                Products item;
//                Integer localQuantity;
//                UC Ht_inst = UC.getInstance();
//
//                item = Ht_inst.getmProductList().get(position);
//                localQuantity = item.getQuantity();
//                localQuantity--;
//                if (localQuantity <= 0) {
//                    localQuantity = 0;
//                    if (Ht_inst.getCartItems().contains(item)) {
//                        int q_count = Ht_inst.getCartqunatity();
//                        q_count--;
//                        Ht_inst.setCartqunatity(q_count);
//                        cart_quantity.setText("" + q_count);
//                        Ht_inst.getCartItems().remove(item);
//                    }
//                }
//                else {
//                    int q_count = Ht_inst.getCartqunatity();
//                    q_count--;
//                    Ht_inst.setCartqunatity(q_count);
//                    cart_quantity.setText("" + q_count);
//                    if(!Ht_inst.getCartItems().contains(item)){
//                        Ht_inst.getCartItems().add(item);
//                    }
//                }
//
//                item.setQuantity(localQuantity);
//                serviceAdapter.notifyDataSetChanged();
//
//                Ht_inst.setAddproducts(item);
//                AsyncTask<Context, Void, Integer> result = new Updatecart().execute(ServicesActivity.this);
//                try {
//
//                    int code = result.get();
//                    //  System.out.println("jyo code" + code);
//                    if(code==200){
//                        Toast.makeText(ServicesActivity.this, "Added successfully", Toast.LENGTH_SHORT).show();
//                    }
//                } catch (Exception e) {
//                    Toast.makeText(ServicesActivity.this, "Application Fail to add", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//
//            @Override
//            public void onAddToCartButtonClick(View v, int position) {
//
//                progressBar.setVisibility(View.VISIBLE);
//                Products item;
//                Integer localQuantity;
//                UC Ht_inst = UC.getInstance();
//
//                //Log.d("jyo ", "plus button clicked");
//                item = UC.getInstance().getmProductList().get(position);
//                localQuantity = item.getQuantity();
//                localQuantity++;
//                item.setQuantity(localQuantity);
//                if (!Ht_inst.getCartItems().contains(item)) {
//                    Ht_inst.getCartItems().add(item);
//                }
//
//                int g_cart = Ht_inst.getCartqunatity();
//                g_cart++;
//                Ht_inst.setCartqunatity(g_cart);
//                cart_quantity.setText("" + g_cart);
//                serviceAdapter.notifyDataSetChanged();
//
//                Ht_inst.setAddproducts(item);
//
//                AsyncTask<Context, Void, Integer> result = new Addcart().execute(ServicesActivity.this);
//                try {
//
//                    int code = result.get();
//                    // System.out.println("jyo code" + code);
//                    if(code==200){
//                        Toast.makeText(ServicesActivity.this, "Added successfully", Toast.LENGTH_SHORT).show();
//                    }
//                } catch (Exception e) {
//                    Toast.makeText(ServicesActivity.this, "Application Fail to add", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//
//        });
//
//
//
//
//












package com.MakemyBeauty.makemybeauty.Network;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NetworkClass {

    public static void MakeRequest(String url, HashMap<String, String> params, Integer type, Context mContext, final VolleyCallback callback){
        final String TAG = "NetworkVolley";
        JSONObject jsonObject = null;
        if (params != null){
            jsonObject = new JSONObject(params);
            Log.v(TAG,"JOBJECT volley "+ jsonObject.toString());
            Log.v(TAG,"URL "+ url);
        }
        JsonObjectRequest req = new JsonObjectRequest(type,url,
                jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.v(TAG,"Response volley qw " + response.toString());
                try {

                    callback.onSuccess(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.v(TAG,"Response volley" + volleyError.toString());
                String err = null;
                if (volleyError instanceof com.android.volley.NoConnectionError){
                    err = "No internet Access!";
                }
                try {
                    if(err != "null") {
                        callback.onError(err);
                    }
                    else {
                        callback.onError(volleyError.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        req.setShouldCache(false);
        VolleyController.getInstance(mContext).addToRequestQueue(req);
    }
}
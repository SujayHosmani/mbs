package com.MakemyBeauty.makemybeauty.Utilities;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.MakemyBeauty.makemybeauty.Model.UC;

public class Dao   {

    SQLiteDatabase db;
    public Dao() {

        if(DbHelper.getInstance() != null){
            db = DbHelper.getInstance().getWritableDatabase();
            db.execSQL("CREATE TABLE IF NOT EXISTS UsrDetails(mobileNum VARCHAR(20), email VARCHAR(20), id INT, token VARCHAR(50),name VARCHAR(50))");

        }else {
            throw new RuntimeException("DBHelper not initialized");
        }
    }

    public void addUserDetails() {
        UC om_inst = UC.getInstance();
        db.delete("UsrDetails", null, null);
        db.execSQL("INSERT INTO UsrDetails VALUES('"+om_inst.getUser().getMobile()+"','"+om_inst.getUser().getEmail()+"','"+om_inst.getUser().getUserId()+"','"+om_inst.getUser().getUserToken()+"','"+om_inst.getUser().getName()+"')");
        //System.out.println("jyo insert db"+om_inst.getUser().getMobile()+"','"+om_inst.getUser().getEmail()+"','"+om_inst.getUser().getUserId()+"','"+om_inst.getUser().getUserToken()+"','"+om_inst.getUser().getName()+"')");

    }

    public void getUserDetails() {
        Cursor cursor = db.rawQuery("Select * from UsrDetails", new String[]{});
        if(cursor.getCount() > 0){
            cursor.moveToFirst();
            UC dss_inst = UC.getInstance();
            System.out.println("jyo get called mob "+dss_inst.getUser().getMobile());
            dss_inst.getUser().setMobile(cursor.getString(0));
            dss_inst.getUser().setEmail(cursor.getString(1));
            dss_inst.getUser().setUserId(cursor.getInt(2));
            dss_inst.getUser().setUserToken(cursor.getString(3));
            dss_inst.getUser().setName(cursor.getString(4));
            //  System.out.println("jyo get called toke "+dss_inst.getUser().getUserToken());
        }
        else {
            UC dss_inst = UC.getInstance();
            dss_inst.getUser().setUserId(-1);
              /* dss_inst.getUser().setMobile(cursor.getString(0));
               dss_inst.getUser().setUserToken(cursor.getString(3));*/
        }
    }
    public void deleteUserDetails () {
        db.delete("UsrDetails", null, null);

    }

}

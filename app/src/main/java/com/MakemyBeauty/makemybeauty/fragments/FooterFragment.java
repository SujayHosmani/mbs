package com.MakemyBeauty.makemybeauty.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.MakemyBeauty.makemybeauty.Activities.BookingActivity;
import com.MakemyBeauty.makemybeauty.Activities.CartActivity;
import com.MakemyBeauty.makemybeauty.Activities.HomeActivity;
import com.MakemyBeauty.makemybeauty.Activities.ProductActivity;
import com.MakemyBeauty.makemybeauty.Activities.ProfileActivity;
import com.MakemyBeauty.makemybeauty.R;


public class FooterFragment extends Fragment {

    View view;
    TextView tvFooter_home, tvFooter_order, tvFooter_product, tvCart, tvFooter_profile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_footerfragment, container, false);


        tvFooter_home = view.findViewById(R.id.tvFooter_home);

        tvFooter_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        tvFooter_order = view.findViewById(R.id.tvFooter_order);
        tvFooter_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BookingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

//
//        tvFooter_product = view.findViewById(R.id.tvFooter_product);
//
//        tvFooter_product.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), ProductActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//            }
//        });

        tvCart = view.findViewById(R.id.tvCart);

        tvCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        tvFooter_profile = view.findViewById(R.id.tvFooter_profile);

        tvFooter_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        return view;
    }
}
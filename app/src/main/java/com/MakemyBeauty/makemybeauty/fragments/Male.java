package com.MakemyBeauty.makemybeauty.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.MakemyBeauty.makemybeauty.Adapter.GridAdapter;
import com.MakemyBeauty.makemybeauty.R;

public class Male extends Fragment {
    View view;
    GridView simplegridView;

     String[] values = {"one","two","three","four","five","six"};
    //int[] imgaes = {R.drawable.number_one,R.drawable.number_two,R.drawable.number_three,R.drawable.number_four,R.drawable.number_one,R.drawable.number_two,};


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_male, container, false);


        simplegridView = view.findViewById(R.id.simplegridView);

//        GridAdapter gridAdapter = new GridAdapter(getContext(), values);
//        simplegridView.setAdapter(gridAdapter);

        return view;
    }


}